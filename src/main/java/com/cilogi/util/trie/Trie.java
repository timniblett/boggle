// Copyright (c) 2013 Cilogi. All Rights Reserved.
//
// File:        Trie.java  (06/08/13)
// Author:      tim
//
// Copyright in the whole and every part of this source file belongs to
// Cilogi (the Author) and may not be used, sold, licenced, 
// transferred, copied or reproduced in whole or in part in 
// any manner or form or in or on any media to any person other than 
// in accordance with the terms of The Author's agreement
// or otherwise without the prior written consent of The Author.  All
// information contained in this source file is confidential information
// belonging to The Author and as such may not be disclosed other
// than in accordance with the terms of The Author's agreement, or
// otherwise, without the prior written consent of The Author.  As
// confidential information this source file must be kept fully and
// effectively secure at all times.
//


package com.cilogi.util.trie;

import com.cilogi.util.trie.compressed.BitWriter;
import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import lombok.Getter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.LinkedList;


public class Trie implements ITrie {
    static final Logger LOG = LoggerFactory.getLogger(Trie.class);

    @Getter
    private final TrieNode root;

    @Getter
    private int nodeCount;

    public Trie() {
        this.root = new TrieNode();
        nodeCount = 1;
    }

    public boolean hasWord(String s) {
        Preconditions.checkNotNull(s, "Input word can't be null");
        Preconditions.checkArgument(s.length() > 0, "word must have at least one character");

        TrieNode node = root;
        if (node != null) {
            for (int i = 0; i < s.length(); i++) {
                char c = Character.toLowerCase(s.charAt(i));
                node = node.find(c);
                if (node == null) {
                    return false;
                }
            }
            return node.isTerminal();
        }
        return false;
    }

    public void addWord(String s) {
        addWord(s, 0, s.length());
    }


    public String encode() {
        final BitWriter bits = new BitWriter();
        bits.write( 0x02, 2 );
        apply(new INodeFn() {
            public void apply(TrieNode node) {
                final int SZ = node.getChildren().size();
                for (int i = 0; i < SZ; i++) {
                    bits.write(1, 1);
                }
                bits.write(0, 1);
            }
        });

         apply(new INodeFn() {
            public void apply(TrieNode node) {
                int value = node.getLetter() - 'a';
                if (node.isTerminal()) {
                    value |= 0x20;
                }
                bits.write(value, 6);
            }
         });

        return bits.getData();
    }

    public void addWord(String s, int start, int len) {
        Preconditions.checkNotNull(s, "String should not be null");
        Preconditions.checkArgument(start >= 0 && start < s.length(), "Start index must be >= 0 and less than " + s.length() + " not " + start);
        Preconditions.checkArgument(len > 1 && start + len <= s.length(), "Length must be > 1 and less or equal " + (s.length() - start) + " not " + len);

        TrieNode node = root;
        for (int i = start; i < start + len; i++) {
            char ch = s.charAt(i);
            TrieNode child = node.find(ch);
            if (child == null) {
                child = new TrieNode(ch);
                node.addChild(child);
                nodeCount++;
            }
            node = child;
        }
        node.setTerminal(true);
    }

    private void apply(INodeFn fn) {
        LinkedList<TrieNode> level = Lists.newLinkedList();
        level.add(root);
        while( level.size() > 0 ) {
            TrieNode node = level.removeFirst();
            for (TrieNode child : node.getChildren()) {
                level.add(child);
            }
            fn.apply(node);
        }
    }


    private static interface INodeFn {
        public void apply(TrieNode node);
    }
}
