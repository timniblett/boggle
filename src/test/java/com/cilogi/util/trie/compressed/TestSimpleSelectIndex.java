// Copyright (c) 2013 Cilogi. All Rights Reserved.
//
// File:        TestSimpleSelectIndex.java  (19/08/13)
// Author:      tim
//
// Copyright in the whole and every part of this source file belongs to
// Cilogi (the Author) and may not be used, sold, licenced, 
// transferred, copied or reproduced in whole or in part in 
// any manner or form or in or on any media to any person other than 
// in accordance with the terms of The Author's agreement
// or otherwise without the prior written consent of The Author.  All
// information contained in this source file is confidential information
// belonging to The Author and as such may not be disclosed other
// than in accordance with the terms of The Author's agreement, or
// otherwise, without the prior written consent of The Author.  As
// confidential information this source file must be kept fully and
// effectively secure at all times.
//


package com.cilogi.util.trie.compressed;

import org.junit.Before;
import org.junit.Test;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.junit.Assert.*;

public class TestSimpleSelectIndex {
    static final Logger LOG = LoggerFactory.getLogger(TestSimpleSelectIndex.class);


    public TestSimpleSelectIndex() {
    }

    @Before
    public void setUp() {

    }

    @Test
    public void testOneChar() {
        BitWriter writer = new BitWriter();
        writer.write(0x0, 6);
        SimpleSelectIndex simpleSelect = new SimpleSelectIndex(writer.getData(), 6);
        int index = simpleSelect.select(1);
        assertEquals(0, index);

        index = simpleSelect.select(4);
        assertEquals(3, index);
    }

    @Test
    public void testFirst() {
        BitWriter writer = new BitWriter();
        writer.write(0x0, 6);
        SimpleSelectIndex simpleSelect = new SimpleSelectIndex(writer.getData(), 6);

        int index = simpleSelect.select(0);
        assertEquals(-1, index);
    }

    @Test
    public void testMultiCharLo() {
        BitWriter writer = new BitWriter();
        writer.write(0x3f, 6);
        writer.write(0x0, 6);
        SimpleSelectIndex simpleSelect = new SimpleSelectIndex(writer.getData(), 12);
        int index = simpleSelect.select(0);
        assertEquals(-1, index);

        index = simpleSelect.select(1);
        assertEquals(6, index);

        index = simpleSelect.select(3);
        assertEquals(8, index);
    }

    @Test
    public void testMultiCharHi() {
        BitWriter writer = new BitWriter();
        for (int i = 0; i < 8; i++) {
            writer.write(0x3, 6);
        }

        writer.write(0x3f, 6);
        writer.write(0x20, 6);
        SimpleSelectIndex simpleSelect = new SimpleSelectIndex(writer.getData(), 60);
        int index = simpleSelect.select(0);
        assertEquals(-1, index);

        index = simpleSelect.select(33);
        assertEquals(55, index);

        // check that rank directory gives same answer...
        RankDirectory dir = RankDirectory.create(writer.getData(), 60);
        int dirIndex = dir.select(0, 33);
        assertEquals(55, dirIndex);


    }
}