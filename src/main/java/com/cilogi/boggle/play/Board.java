// Copyright (c) 2013 Cilogi. All Rights Reserved.
//
// File:        Board.java  (06/08/13)
// Author:      tim
//
// Copyright in the whole and every part of this source file belongs to
// Cilogi (the Author) and may not be used, sold, licenced, 
// transferred, copied or reproduced in whole or in part in 
// any manner or form or in or on any media to any person other than 
// in accordance with the terms of The Author's agreement
// or otherwise without the prior written consent of The Author.  All
// information contained in this source file is confidential information
// belonging to The Author and as such may not be disclosed other
// than in accordance with the terms of The Author's agreement, or
// otherwise, without the prior written consent of The Author.  As
// confidential information this source file must be kept fully and
// effectively secure at all times.
//


package com.cilogi.boggle.play;

import com.google.common.base.Preconditions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class Board {
    static final Logger LOG = LoggerFactory.getLogger(Board.class);


    static final int[][] ADJACENT = {
            {1, 4, 5},
            {2, 6, 5, 4, 0},
            {3, 7, 6, 5, 1},
            {7, 6, 2},

            {0, 1, 5, 9, 8},
            {1, 2, 6, 10, 9, 8, 4, 0},
            {2, 3, 7, 11, 10, 9, 5, 1},
            {3, 11, 10, 6, 2},

            {4, 5, 9, 13, 12},
            {5, 6, 10, 14, 13, 12, 8, 4},
            {6, 7, 11, 15, 14, 13, 9, 5},
            {7, 15, 14, 10, 6},

            {8, 9, 13},
            {9, 10, 14, 12, 8},
            {10, 11, 15, 13, 9},
            {11, 14, 10},
    };

    private static final String[] DEFAULT_VALUES =
            {
                    "T", "H", "E", "X",
                    "X","X","X","X",
                    "X","X","X","X",
                    "X","X","X","X"
            };


    private final String[] values;

    public Board() {
        this(DEFAULT_VALUES);
    }

    public Board(String[] values) {
        this.values = values;
    }

    public String getValue(int location) {
        return "" + values[location];
    }

    public int size() {
        return ADJACENT.length;
    }

    public int[] adjacent(int location) {
        Preconditions.checkArgument(location >= 0 && location < size());
        return ADJACENT[location];
    }


}
