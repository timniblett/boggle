// Copyright (c) 2013 Cilogi. All Rights Reserved.
//
// File:        StringIntArray.java  (22/08/13)
// Author:      tim
//
// Copyright in the whole and every part of this source file belongs to
// Cilogi (the Author) and may not be used, sold, licenced, 
// transferred, copied or reproduced in whole or in part in 
// any manner or form or in or on any media to any person other than 
// in accordance with the terms of The Author's agreement
// or otherwise without the prior written consent of The Author.  All
// information contained in this source file is confidential information
// belonging to The Author and as such may not be disclosed other
// than in accordance with the terms of The Author's agreement, or
// otherwise, without the prior written consent of The Author.  As
// confidential information this source file must be kept fully and
// effectively secure at all times.
//


package com.cilogi.util.trie.compressed;

import com.google.common.base.Preconditions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * An array of integers stored in Base64, 4 chars per int.
 */
public class StringIntArray {
    static final Logger LOG = LoggerFactory.getLogger(StringIntArray.class);

    private StringBuilder builder;

    public StringIntArray() {
        this.builder = new StringBuilder();
    }

    public StringIntArray set(String s) {
        builder = new StringBuilder(s);
        return this;
    }

    public void add(int val) {
        char[] vals = Base64.i2c(val);
        builder.append(vals);
    }

    public int size() {
        return builder.length()/4;
    }

    public int get(int index) {
        Preconditions.checkArgument(index >= 0 && builder.length() > index * 4);
        return Base64.c2i(builder, index*4);
    }

    @Override
    public String toString() {
        return builder.toString();
    }

}
