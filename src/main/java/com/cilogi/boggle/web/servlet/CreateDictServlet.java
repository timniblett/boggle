// Copyright (c) 2013 Cilogi. All Rights Reserved.
//
// File:        CreateDictServlet.java  (30/08/13)
// Author:      tim
//
// Copyright in the whole and every part of this source file belongs to
// Cilogi (the Author) and may not be used, sold, licenced, 
// transferred, copied or reproduced in whole or in part in 
// any manner or form or in or on any media to any person other than 
// in accordance with the terms of The Author's agreement
// or otherwise without the prior written consent of The Author.  All
// information contained in this source file is confidential information
// belonging to The Author and as such may not be disclosed other
// than in accordance with the terms of The Author's agreement, or
// otherwise, without the prior written consent of The Author.  As
// confidential information this source file must be kept fully and
// effectively secure at all times.
//


package com.cilogi.boggle.web.servlet;

import com.cilogi.boggle.web.util.CreateDoc;
import com.cilogi.util.trie.Generate;
import com.cilogi.util.trie.Trie;
import com.cilogi.util.trie.compressed.GenerateCompressed;
import com.google.appengine.api.blobstore.BlobInfo;
import com.google.appengine.api.blobstore.BlobKey;
import com.google.common.base.Charsets;
import com.google.common.collect.Maps;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Singleton
public class CreateDictServlet extends BaseServlet {
    static final Logger LOG = LoggerFactory.getLogger(CreateDictServlet.class);

    @Inject
    public CreateDictServlet(CreateDoc create) {
        super(create);
    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        Map<String,String> map = Maps.newHashMap();
        map.put("url", blobService.createUploadUrl("/create"));
        byte[] out = createDoc.createDocument("create.ftl", map);
        issue(MimeTypes.MIME_TEXT_HTML, 200, out, response);

        response.setHeader("Cache-Control", "private, no-store, no-cache, must-revalidate");
        response.setHeader("Pragma", "no-cache");
    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        Map<String,List<BlobInfo>> blobInfos = blobService.getBlobInfos(request);
        BlobInfo info = blobInfoOf("dictionary", blobInfos);
        if (info != null) {
            long sz = info.getSize();
            response.sendRedirect("/download?key="+info.getBlobKey().getKeyString()+"&sz="+sz);
        } else {
            issue(MimeTypes.MIME_TEXT_PLAIN, 500, "Can't load dictionary file", response);
        }
    }

}
