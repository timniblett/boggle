// Copyright (c) 2013 Cilogi. All Rights Reserved.
//
// File:        DownloadDictServlet.java  (31/08/13)
// Author:      tim
//
// Copyright in the whole and every part of this source file belongs to
// Cilogi (the Author) and may not be used, sold, licenced, 
// transferred, copied or reproduced in whole or in part in 
// any manner or form or in or on any media to any person other than 
// in accordance with the terms of The Author's agreement
// or otherwise without the prior written consent of The Author.  All
// information contained in this source file is confidential information
// belonging to The Author and as such may not be disclosed other
// than in accordance with the terms of The Author's agreement, or
// otherwise, without the prior written consent of The Author.  As
// confidential information this source file must be kept fully and
// effectively secure at all times.
//


package com.cilogi.boggle.web.servlet;

import com.cilogi.boggle.web.util.CreateDoc;
import com.cilogi.util.trie.Generate;
import com.cilogi.util.trie.Trie;
import com.cilogi.util.trie.compressed.GenerateCompressed;
import com.google.appengine.api.blobstore.BlobKey;
import com.google.common.base.Charsets;
import com.google.common.collect.Maps;
import com.google.inject.Inject;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Singleton;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

@Singleton
public class DownloadDictServlet extends BaseServlet {
    static final Logger LOG = LoggerFactory.getLogger(DownloadDictServlet.class);


    @Inject
    public DownloadDictServlet(CreateDoc create) {
        super(create);
    }
    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String keyName = request.getParameter("key");
        long sz = Long.parseLong(request.getParameter("sz"));
        Map<String,String> map = Maps.newHashMap();
        map.put("key", keyName);
        map.put("sz", Long.toString(sz));
        byte[] out = createDoc.createDocument("download.ftl", map);
        issue(MimeTypes.MIME_TEXT_HTML, 200, out, response);
    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String keyName = request.getParameter("key");
        long sz = Long.parseLong(request.getParameter("sz"));
        if (keyName != null & sz > 0) {
            BlobKey key = new BlobKey(keyName);
            try {
                byte[] data = blobService.fetchData(key, 0L, sz);
                String dict = new String(data, Charsets.UTF_8);
                Trie trie = Generate.trie(dict);
                JSONObject json = new GenerateCompressed().compressJSON(trie);
                String jsonString = json.toJSONString();
                String out = "define([], " + jsonString +  ");\n";

                response.setHeader("Content-Disposition", "attachment; filename=\"dict.js\"");
                issue(MimeTypes.MIME_APPLICATION_JS, 200, out, response);
            } catch (Exception e) {
                issue(MimeTypes.MIME_TEXT_PLAIN, 500, "Internal error accessing dictionary: " + e.getMessage(), response);
            }
            blobService.delete(key);
        } else {
            response.sendRedirect("/");
        }
    }

}
