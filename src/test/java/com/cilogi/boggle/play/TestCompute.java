// Copyright (c) 2013 Cilogi. All Rights Reserved.
//
// File:        TestCompute.java  (06/08/13)
// Author:      tim
//
// Copyright in the whole and every part of this source file belongs to
// Cilogi (the Author) and may not be used, sold, licenced, 
// transferred, copied or reproduced in whole or in part in 
// any manner or form or in or on any media to any person other than 
// in accordance with the terms of The Author's agreement
// or otherwise without the prior written consent of The Author.  All
// information contained in this source file is confidential information
// belonging to The Author and as such may not be disclosed other
// than in accordance with the terms of The Author's agreement, or
// otherwise, without the prior written consent of The Author.  As
// confidential information this source file must be kept fully and
// effectively secure at all times.
//


package com.cilogi.boggle.play;

import com.cilogi.util.trie.Generate;
import com.cilogi.util.trie.Trie;
import org.junit.Before;
import org.junit.Test;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Set;

import static org.junit.Assert.*;

public class TestCompute {
    static final Logger LOG = LoggerFactory.getLogger(TestCompute.class);


    public TestCompute() {
    }

    @Before
    public void setUp() {

    }

    @Test
    public void testCompute() {
        Trie trie = Generate.trie();
        Compute compute = new Compute(trie, new Board());
        compute.compute();
        Set<String> words = compute.getWords();
        assertEquals(5, words.size());
    }
}