// Copyright (c) 2013 Cilogi. All Rights Reserved.
//
// File:        DemoCompute.java  (07/08/13)
// Author:      tim
//
// Copyright in the whole and every part of this source file belongs to
// Cilogi (the Author) and may not be used, sold, licenced, 
// transferred, copied or reproduced in whole or in part in 
// any manner or form or in or on any media to any person other than 
// in accordance with the terms of The Author's agreement
// or otherwise without the prior written consent of The Author.  All
// information contained in this source file is confidential information
// belonging to The Author and as such may not be disclosed other
// than in accordance with the terms of The Author's agreement, or
// otherwise, without the prior written consent of The Author.  As
// confidential information this source file must be kept fully and
// effectively secure at all times.
//


package com.cilogi.boggle.play;

import com.cilogi.util.trie.Generate;
import com.cilogi.util.trie.Trie;
import com.cilogi.util.trie.TrieNode;
import com.google.common.collect.Lists;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Set;


public class DemoCompute {
    static final Logger LOG = LoggerFactory.getLogger(DemoCompute.class);


    public DemoCompute(String[] vals) {
        Trie trie = Generate.trie();
        Board board = new Board(vals);
        Compute compute = new Compute(trie, board);
        long start = System.currentTimeMillis();
        compute.compute();
        long stop = System.currentTimeMillis() - start;
        LOG.info("compute takes " + stop + "ms");
        Set<String> words = compute.getWords();
        LOG.info("# words " + words.size());
        List<String> w = Lists.newArrayList(words);
        Collections.sort(w, new Comparator<String>() {
            public int compare(String s0, String s1) {
                return s1.length() - s0.length();
            }
        });
        for (String word : w) {
            if (word.length() > 2) {
                System.out.println(word);
            }
        }
    }

    public static void main(String[] args) {
        try {
            String[] vals = {
                "n","r","t","f",
                "n","a","r","u",
                "x","e","s","e",
                "d","t","a","t",
            };
            new DemoCompute(vals);
        } catch (Exception e) {
            LOG.error("oops", e);
        }
    }
}
