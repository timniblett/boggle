<script src="http://code.jquery.com/jquery-1.10.1.min.js"></script>
<script>
    $("#menuLink").click(function(e) {
        $("#menuLink, #menu, #layout").toggleClass("active");
        e.preventDefault();
    })
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-27131467-7', 'solveboggle.appspot.com');
  ga('send', 'pageview');

</script>