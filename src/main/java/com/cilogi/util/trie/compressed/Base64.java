// Copyright (c) 2013 Cilogi. All Rights Reserved.
//
// File:        Base64.java  (18/08/13)
// Author:      tim
//
// Copyright in the whole and every part of this source file belongs to
// Cilogi (the Author) and may not be used, sold, licenced, 
// transferred, copied or reproduced in whole or in part in 
// any manner or form or in or on any media to any person other than 
// in accordance with the terms of The Author's agreement
// or otherwise without the prior written consent of The Author.  All
// information contained in this source file is confidential information
// belonging to The Author and as such may not be disclosed other
// than in accordance with the terms of The Author's agreement, or
// otherwise, without the prior written consent of The Author.  As
// confidential information this source file must be kept fully and
// effectively secure at all times.
//


package com.cilogi.util.trie.compressed;

import com.google.common.collect.Maps;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;


class Base64 {
    static final Logger LOG = LoggerFactory.getLogger(Base64.class);

    private static final String BASE64 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_";

    private static final Map<Character, Integer> BASE64_CACHE = Maps.newHashMap();

    static {
        for (int i = 0; i < BASE64.length(); i++) {
            BASE64_CACHE.put(BASE64.charAt(i), i);
        }
    }


    private static final int[] BITS_PER_BYTE = {
        0, 1, 1, 2, 1, 2, 2, 3, 1, 2, 2, 3, 2, 3, 3, 4, 1, 2, 2, 3, 2, 3, 3, 4, 2,
        3, 3, 4, 3, 4, 4, 5, 1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5, 2, 3,
        3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6, 1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3,
        4, 3, 4, 4, 5, 2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6, 2, 3, 3, 4,
        3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6, 3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5,
        6, 6, 7, 1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5, 2, 3, 3, 4, 3, 4,
        4, 5, 3, 4, 4, 5, 4, 5, 5, 6, 2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5,
        6, 3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7, 2, 3, 3, 4, 3, 4, 4, 5,
        3, 4, 4, 5, 4, 5, 5, 6, 3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7, 3,
        4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7, 4, 5, 5, 6, 5, 6, 6, 7, 5, 6,
        6, 7, 6, 7, 7, 8
    };

    private static final int[] ORD = new int[256];
    static  {
        for (int i = 0; i < BASE64.length(); i++) {
            int cVal = (int)BASE64.charAt(i);
            ORD[cVal] = i;
        }
    }

    private static final int[] BITS_PER_CHAR = new int[256];
    static  {
        for (int i = 0; i < BASE64.length(); i++) {
            int cVal = (int)BASE64.charAt(i);
            int nBits = BITS_PER_BYTE[i];
            BITS_PER_CHAR[cVal] = nBits;
        }
    }


    Base64() {

    }



    // 24-bit conversion of int to Base64
    static char[] i2c(int val) {
        final int ONES = 0x3f;
        final int WIDTH = 6;

        char[] chars = new char[4];
        chars[0] = BASE64.charAt(((ONES << (WIDTH*3)) & val)>>>(WIDTH*3));
        chars[1] = BASE64.charAt(((ONES << (WIDTH*2)) & val)>>>(WIDTH*2));
        chars[2] = BASE64.charAt(((ONES << WIDTH) & val)>>>WIDTH);
        chars[3] = BASE64.charAt(ONES & val);
        return chars;
    }

    static int c2i(CharSequence s, int offset) {
        int val = 0;
        for (int i = 0; i < 4; i++) {
            char c = s.charAt(offset + i);
            int cVal = ORD[c];
            val = (val << 6) | cVal;
        }
        return val;
    }

    static int width() {
        return  6;
    }

    static int bits(char c) {
        return BITS_PER_CHAR[(int)c];
    }

    static int value(char c) {
        return BASE64_CACHE.get(c);
    }
}
