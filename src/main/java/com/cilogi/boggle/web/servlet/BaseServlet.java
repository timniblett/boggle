// Copyright (c) 2012 Tim Niblett. All Rights Reserved.
//
// File:        BaseServlet.java  (11-Sep-2012)
// Author:      tim
//
// Copyright in the whole and every part of this source file belongs to
// Tim Niblett (the Author) and may not be used, sold, licenced, 
// transferred, copied or reproduced in whole or in part in 
// any manner or form or in or on any media to any person other than 
// in accordance with the terms of The Author's agreement
// or otherwise without the prior written consent of The Author.  All
// information contained in this source file is confidential information
// belonging to The Author and as such may not be disclosed other
// than in accordance with the terms of The Author's agreement, or
// otherwise, without the prior written consent of The Author.  As
// confidential information this source file must be kept fully and
// effectively secure at all times.
//


package com.cilogi.boggle.web.servlet;

import com.cilogi.boggle.web.util.CreateDoc;
import com.google.appengine.api.blobstore.BlobInfo;
import com.google.appengine.api.blobstore.BlobKey;
import com.google.appengine.api.blobstore.BlobstoreService;
import com.google.appengine.api.blobstore.BlobstoreServiceFactory;
import com.google.appengine.api.images.ImagesService;
import com.google.appengine.api.images.ImagesServiceFactory;
import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;
import com.google.common.base.Preconditions;
import org.json.simple.JSONObject;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;


public abstract class BaseServlet extends HttpServlet {
    static final Logger LOG = Logger.getLogger(BaseServlet.class.getName());


    protected final CreateDoc createDoc;
    protected final UserService userService;
    protected final BlobstoreService blobService;
    protected final ImagesService imagesService;

    public BaseServlet(CreateDoc createDoc) {
        this.createDoc = createDoc;
        userService = UserServiceFactory.getUserService();
        blobService = BlobstoreServiceFactory.getBlobstoreService();
        imagesService = ImagesServiceFactory.getImagesService();
    }

    protected void issue(String mimeType, int returnCode, byte[] output, HttpServletResponse response) throws IOException {
        response.setContentType(mimeType);
        response.setStatus(returnCode);
        response.getOutputStream().write(output);
    }

    protected void issue(String mimeType, int returnCode, String output, HttpServletResponse response) throws IOException {
        response.setContentType(mimeType);
        response.setStatus(returnCode);
        response.getWriter().println(output);
    }

    protected void issueJson(int returnCode, HttpServletResponse response, Object... args) throws IOException {
        JSONObject obj = obj2json(args);
        issue(MimeTypes.MIME_APPLICATION_JSON, returnCode, obj.toJSONString(), response);
    }


    protected static BlobInfo blobInfoOf(String key, Map<String,List<BlobInfo>> blobs) {
        List<BlobInfo> list = blobs.get(key);
        return (list.size() == 0) ? null : list.get(0);
    }

    private static JSONObject obj2json(Object... args) {
        Preconditions.checkArgument(args.length % 2 == 0, "Even number of arguments, not " + args.length);
        JSONObject out = new JSONObject();
        for (int i = 0; i < args.length; i += 2) {
                out.put(args[i], args[i+1]);
        }
        return out;
    }
}
