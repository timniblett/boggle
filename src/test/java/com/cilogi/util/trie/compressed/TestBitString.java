// Copyright (c) 2013 Cilogi. All Rights Reserved.
//
// File:        TestBitString.java  (10/08/13)
// Author:      tim
//
// Copyright in the whole and every part of this source file belongs to
// Cilogi (the Author) and may not be used, sold, licenced, 
// transferred, copied or reproduced in whole or in part in 
// any manner or form or in or on any media to any person other than 
// in accordance with the terms of The Author's agreement
// or otherwise without the prior written consent of The Author.  All
// information contained in this source file is confidential information
// belonging to The Author and as such may not be disclosed other
// than in accordance with the terms of The Author's agreement, or
// otherwise, without the prior written consent of The Author.  As
// confidential information this source file must be kept fully and
// effectively secure at all times.
//


package com.cilogi.util.trie.compressed;

import org.junit.Before;
import org.junit.Test;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.junit.Assert.*;

public class TestBitString {
    static final Logger LOG = LoggerFactory.getLogger(TestBitString.class);


    public TestBitString() {
    }

    @Before
    public void setUp() {

    }

    @Test
    public void testSingleCharAllSet() {
        BitWriter writer = new BitWriter();
        writer.write(0xffffffff, 32);
        String out = writer.getData();

        BitString bs = new BitString(out);
        int nBits = bs.count(0, 32);
        assertEquals(32, nBits);
    }

    @Test
    public void testSplit() {
        BitWriter writer = new BitWriter();
        writer.write(0x0000ffff, 32);
        writer.write(0xffff0000, 32);
        String out = writer.getData();

        BitString bs = new BitString(out);
        int nBits = bs.count(0, 64);
        assertEquals(nBits, 32);
    }

    @Test
    public void testGet() {
        BitWriter writer = new BitWriter();
        writer.write(0xffffffff, 32);
        writer.write(0x00000000d, 32);
        String out = writer.getData();

        BitString bs = new BitString(out);
        int val = bs.get(32 + 27, 5);
        assertEquals(13, val);
    }

    @Test
    public void testEmptyChar() {
        BitWriter writer = new BitWriter();
        writer.write(0x0, 6);
        String out = writer.getData();
        assertEquals("A", out);
    }

}