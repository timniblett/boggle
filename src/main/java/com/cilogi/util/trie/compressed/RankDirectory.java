// Copyright (c) 2013 Cilogi. All Rights Reserved.
//
// File:        RankDirectory.java  (10/08/13)
// Author:      tim
//
// Copyright in the whole and every part of this source file belongs to
// Cilogi (the Author) and may not be used, sold, licenced, 
// transferred, copied or reproduced in whole or in part in 
// any manner or form or in or on any media to any person other than 
// in accordance with the terms of The Author's agreement
// or otherwise without the prior written consent of The Author.  All
// information contained in this source file is confidential information
// belonging to The Author and as such may not be disclosed other
// than in accordance with the terms of The Author's agreement, or
// otherwise, without the prior written consent of The Author.  As
// confidential information this source file must be kept fully and
// effectively secure at all times.
//


package com.cilogi.util.trie.compressed;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


class RankDirectory {
    static final Logger LOG = LoggerFactory.getLogger(RankDirectory.class);

    static final int L2 = 32;
    static final int L1 = L2 * L2;

    static RankDirectory create(String data, int numBits) {
        return create(data, numBits, L1, L2);
    }

    static RankDirectory create( String data, int numBits, int l1Size, int l2Size ) {
        BitString bits = new BitString( data );
        int p = 0;
        int i = 0;
        int count1 = 0, count2 = 0;
        int l1bits = (int)Math.ceil( Math.log( numBits ) / Math.log(2) );
        int l2bits = (int)Math.ceil( Math.log( l1Size ) / Math.log(2) );

        BitWriter directory = new BitWriter();

        while( p + l2Size <= numBits ) {
            count2 += bits.count( p, l2Size );
            i += l2Size;
            p += l2Size;
            if ( i == l1Size ) {
                count1 += count2;
                directory.write( count1, l1bits );
                count2 = 0;
                i = 0;
            } else {
                directory.write( count2, l2bits );
            }
        }

        return new RankDirectory( directory.getData(), data, numBits, l1Size, l2Size );
    };

    private BitString directory;
    private BitString data;
    private int l1Size;
    private int l2Size;
    private int l1Bits;
    private int l2Bits;
    private int sectionBits;
    private int numBits;

    RankDirectory(String directoryData, String bitData, int numBits, int l1Size, int l2Size ) {
        this.directory = new BitString( directoryData );
        this.data = new BitString( bitData );
        this.l1Size = l1Size;
        this.l2Size = l2Size;
        this.l1Bits = (int)Math.ceil( Math.log( numBits ) / Math.log( 2 ) );
        this.l2Bits = (int)Math.ceil( Math.log( l1Size ) / Math.log( 2 ) );
        this.sectionBits = (l1Size / l2Size - 1) * this.l2Bits + this.l1Bits;
        this.numBits = numBits;

    }


    /**
     Returns the string representation of the directory.
     @return the string rep of this directory
     */
    String getData() {
        return this.directory.getData();
    }

    /**
      Returns the number of 1 or 0 bits (depending on the "which" parameter) to
      to and including position x.
      */
    int rank(int which, int x) {

        if ( which == 0 ) {
            return x - this.rank( 1, x ) + 1;
        }

        int rank = 0;
        int o = x;
        int sectionPos = 0;

        if ( o >= this.l1Size ) {
            sectionPos = ( o / this.l1Size | 0 ) * this.sectionBits;
            rank = this.directory.get( sectionPos - this.l1Bits, this.l1Bits );
            o = o % this.l1Size;
        }

        if ( o >= this.l2Size ) {
            sectionPos += ( o / this.l2Size | 0 ) * this.l2Bits;
            rank += this.directory.get( sectionPos - this.l2Bits, this.l2Bits );
        }

        rank += this.data.count( x - x % this.l2Size, x % this.l2Size + 1 );

        return rank;
    }

    /**
      Returns the position of the y'th 0 or 1 bit, depending on the "which"
      parameter.
      */
    int select(int which, int y) {
        int high = this.numBits;
        int low = -1;
        int val = -1;

        while ( high - low > 1 ) {
            int probe = (high + low) / 2 | 0;
            int r = this.rank( which, probe );

            if ( r == y ) {
                // We have to continue searching after we have found it,
                // because we want the _first_ occurrence.
                val = probe;
                high = probe;
            } else if ( r < y ) {
                low = probe;
            } else {
                high = probe;
            }
        }

        return val;
    }
}
