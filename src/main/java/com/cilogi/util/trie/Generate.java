// Copyright (c) 2013 Cilogi. All Rights Reserved.
//
// File:        GenerateCompressed.java  (06/08/13)
// Author:      tim
//
// Copyright in the whole and every part of this source file belongs to
// Cilogi (the Author) and may not be used, sold, licenced, 
// transferred, copied or reproduced in whole or in part in 
// any manner or form or in or on any media to any person other than 
// in accordance with the terms of The Author's agreement
// or otherwise without the prior written consent of The Author.  All
// information contained in this source file is confidential information
// belonging to The Author and as such may not be disclosed other
// than in accordance with the terms of The Author's agreement, or
// otherwise, without the prior written consent of The Author.  As
// confidential information this source file must be kept fully and
// effectively secure at all times.
//


package com.cilogi.util.trie;

import com.cilogi.util.IOUtil;
import com.google.common.collect.Lists;
import lombok.Getter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.List;

/**
 * GenerateCompressed a trie from a large space-separated string
 */
public class Generate {
    static final Logger LOG = LoggerFactory.getLogger(Generate.class);

    @Getter
    private final Trie trie;

    private int index;
    private int length;
    private final String dictionary;
    private final int LEN;

    public static Trie trie() {
        String dict = dict();
        return new Generate(dict).generate().getTrie();
    }

    public static Trie trie(String dict) {
        Generate gen = new Generate(dict);
        gen.generate();
        return gen.getTrie();
    }

    private static String dict() {
        URL url = Generate.class.getResource("/words.txt.gz");
        try {
            byte[] data = IOUtil.gunzip(IOUtil.loadBytes(url));
            return new String(data, Charset.forName("utf-8"));

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static List<String> wordList() {
        return new Generate(dict()).words();
    }

    public Generate(String dictionary) {
        this.trie = new Trie();
        this.dictionary = dictionary;
        this.LEN = dictionary.length();
        this.index = 0;
        this.length = 0;
    }

    public Generate generate() {
        long start = System.currentTimeMillis();
        int count = 0;
        while (next()) {
            String s = word();
            trie.addWord(dictionary, index, length);
            count++;
        }
        long tm = System.currentTimeMillis() - start;
        LOG.info("There are " + count + " words at " + tm + "ms");
        return this;
    }

    // debug
    List<String> words() {
        char[] chars = dictionary.toCharArray();
        List<String> out = Lists.newArrayList();
        while (next()) {
            String s = new String(chars, index, length);
            out.add(s);
        }
        return out;
    }


    private boolean next() {
        index += length;
        length = 0;
        for (;index < LEN && !Character.isLetter(dictionary.charAt(index)); index++);
        if (index == LEN) {
            return false;
        }
        int i = index;
        for (;i < LEN && Character.isLetter(dictionary.charAt(i)); i++);
        length = i - index;
        return length > 0;
    }

    private String word() {
        StringBuffer s = new StringBuffer();
        for (int i = 0; i < length; i++) {
            s.append(dictionary.charAt(index + i));
        }
        return s.toString();
    }

}
