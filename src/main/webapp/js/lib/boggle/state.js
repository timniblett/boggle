define(["underscore", "boggle/board"], function(_, Board) {

    function State(node, values) {
        this.visited = [];
        this.prefix = "";
        this.location = -1;
        this.node = node;
        this.board = new Board(values);
    }

    State.count = 0;

    State.prototype.newState =  function(location) {
        var state = new State(), chars = this.board.getValue(location);
        state.board = this.board;
        state.prefix = this.prefix + chars;
        state.visited = this.visited.slice(0);
        state.visited.push(location);
        state.location = location;
        state.node = addChars(this.node, chars);
        if (state.node) {
            state.prefix = this.prefix + chars;
        } else {
            state.prefix = this.prefix + "+";
        }
        State.count++;
        return state;
    }

    State.prototype.isTerminal = function() {
        return this.node ? this.node.final : false;
    }

    State.prototype.hasVisited = function(location) {
        return _.contains(this.visited, location);
    }

    State.prototype.neighbours = function() {
        return (this.location == -1) ? [] : Board.adjacent(location);
    }

    function addChars(root, chars) {
        var i, char, node = root;
        for (i = 0; i < chars.length && node; i++) {
            char = chars.charAt(i).toLowerCase();
            node = node.find(char);
        }
        return node;
    }

    return State;

});