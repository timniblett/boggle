// Copyright (c) 2013 Cilogi. All Rights Reserved.
//
// File:        TrieNode.java  (06/08/13)
// Author:      tim
//
// Copyright in the whole and every part of this source file belongs to
// Cilogi (the Author) and may not be used, sold, licenced, 
// transferred, copied or reproduced in whole or in part in 
// any manner or form or in or on any media to any person other than 
// in accordance with the terms of The Author's agreement
// or otherwise without the prior written consent of The Author.  All
// information contained in this source file is confidential information
// belonging to The Author and as such may not be disclosed other
// than in accordance with the terms of The Author's agreement, or
// otherwise, without the prior written consent of The Author.  As
// confidential information this source file must be kept fully and
// effectively secure at all times.
//


package com.cilogi.util.trie;

import com.google.common.collect.Lists;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * TrieNode for our simple-minded trie.  Set up to generate the trie,
 * which will then get compressed.
 */
public class TrieNode implements ITrieNode {
    static final Logger LOG = LoggerFactory.getLogger(TrieNode.class);

    private static final List<TrieNode> EMPTY_CHILDREN = Lists.newLinkedList();

    @Getter
    private char letter;

    @Getter
    @Setter
    private boolean isTerminal;

    private List<TrieNode> children;

    TrieNode() {
        this(' ');
    }

    TrieNode(char c) {
        this.letter = Character.toLowerCase(c);
        this.isTerminal = false;
        this.children = null;
    }

    public List<TrieNode> getChildren() {
        return (children == null) ? EMPTY_CHILDREN : Collections.unmodifiableList(children);
    }

    @Override
    public TrieNode find(char c) {
        char lc = Character.toLowerCase(c);
        for (TrieNode child : getChildren()) {
            if (child.getLetter() == lc) {
                return child;
            }
        }
        return null;
    }

    void addChild(TrieNode node) {
        if (children == null) {
            children = Lists.newLinkedList();
        }
        children.add(node);
    }


    @Override
    public String toString() {
        return "(TrieNode " + letter + ":" + isTerminal + ":" + (children == null) + ")";
    }
}
