define(["jquery", "underscore", "vendor/mustache", "log"], function ($, _, Mustache, log) {
    var urlTemplate = "http://api.wordnik.com/v4/word.json/{{word}}/definitions?limit={{limit}}&includeRelated=false" +
            "&sourceDictionaries=wiktionary&useCanonical=true&includeTags=false&api_key={{apiKey}}";


    function loadDictRemote(url, success, error) {
        success([{partOfSpeech: "noun", text: "The meaning of the word you asked for"}]);
    }

    function loadDict(url, success, error) {
        $.ajax({
            dataType: "jsonp",
            url: url,
            success: success,
            error: error,
            cache: true,
            timeout:5000
        });
    }

    return function (word, options) {
        var url, opt = _.extend({
                    word: word,
                    limit: 1,
                    apiKey: "fa14ac42a282645c1690a09b1cf0ebc48e5b982098463e968",
                    error: function (xhr, status, text) {
                        log("Couldn't get " + word + ": status " + status + " err " + text);
                    }
                }, options);
        url = Mustache.render(urlTemplate, opt);
        loadDict(url, opt.success, opt.error);
    }
});