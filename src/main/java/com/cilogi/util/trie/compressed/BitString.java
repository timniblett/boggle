// Copyright (c) 2013 Cilogi. All Rights Reserved.
//
// File:        BitString.java  (09/08/13)
// Author:      tim
//
// Copyright in the whole and every part of this source file belongs to
// Cilogi (the Author) and may not be used, sold, licenced, 
// transferred, copied or reproduced in whole or in part in 
// any manner or form or in or on any media to any person other than 
// in accordance with the terms of The Author's agreement
// or otherwise without the prior written consent of The Author.  All
// information contained in this source file is confidential information
// belonging to The Author and as such may not be disclosed other
// than in accordance with the terms of The Author's agreement, or
// otherwise, without the prior written consent of The Author.  As
// confidential information this source file must be kept fully and
// effectively secure at all times.
//


package com.cilogi.util.trie.compressed;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** Count the number of bits in a BASE64-coded string */
public class BitString {
    static final Logger LOG = LoggerFactory.getLogger(BitString.class);

    // the number of bits in a BASE64 character
    private static final int W = 6;

    private static final int[] BITS_PER_BYTE = {
        0, 1, 1, 2, 1, 2, 2, 3, 1, 2, 2, 3, 2, 3, 3, 4, 1, 2, 2, 3, 2, 3, 3, 4, 2,
        3, 3, 4, 3, 4, 4, 5, 1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5, 2, 3,
        3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6, 1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3,
        4, 3, 4, 4, 5, 2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6, 2, 3, 3, 4,
        3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6, 3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5,
        6, 6, 7, 1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5, 2, 3, 3, 4, 3, 4,
        4, 5, 3, 4, 4, 5, 4, 5, 5, 6, 2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5,
        6, 3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7, 2, 3, 3, 4, 3, 4, 4, 5,
        3, 4, 4, 5, 4, 5, 5, 6, 3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7, 3,
        4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7, 4, 5, 5, 6, 5, 6, 6, 7, 5, 6,
        6, 7, 6, 7, 7, 8
    };

    private static final int[] MASK_TOP = {
        0x3f, 0x1f, 0x0f, 0x07, 0x03, 0x01, 0x00
    };

    private String bytes;


    public BitString(String s) {
        this.bytes = s;
    }

    public String getData() {
        return bytes;
    }

    /**
        Returns a decimal number, consisting of a certain number, nBits, of bits
        starting at a certain position, position.
     */
    public int get(int position, int nBits) {

        // case 1: bits lie within the given byte
        if ( ( position % W ) + nBits <= W ) {
            return ( locate(position) & MASK_TOP[ position % W ] ) >> ( W - position % W - nBits );

        // case 2: bits lie incompletely in the given byte
        } else {
            int result = ( locate(position) & MASK_TOP[ position % W ] );

            int l = W - position % W;
            position += l;
            nBits -= l;

            while ( nBits >= W ) {
                result = (result << W) | locate(position);
                position += W;
                nBits -= W;
            }

            if ( nBits > 0 ) {
                result = (result << nBits) | ( locate(position) >>
                    ( W - nBits ) );
            }

            return result;
        }
    }

    /**
        Counts the number of bits set to 1 starting at position p and
        ending at position p + n
     */
    public int count(int p, int n) {

        int count = 0;
        while( n >= 8 ) {
            count += BITS_PER_BYTE[ get( p, 8 ) ];
            p += 8;
            n -= 8;
        }

        return count + BITS_PER_BYTE[ get( p, n ) ];
    }

    /**
        Returns the number of bits set to 1 up to and including position x.
        This is the slow implementation used for testing.
    */
    int rank( int x ) {
        int rank = 0;
        for( int i = 0; i <= x; i++ ) {
            if (get(i, 1) != 0) {
                rank++;
            }
        }

        return rank;
    }

    private int locate(int p) {
        int index = p / W;
        if (index >= bytes.length()) {
            index = 0;
        }
        return BitWriter.ORD(bytes.charAt(index));
    }
}
