// Copyright (c) 2013 Cilogi. All Rights Reserved.
//
// File:        CompressedTrie.java  (12/08/13)
// Author:      tim
//
// Copyright in the whole and every part of this source file belongs to
// Cilogi (the Author) and may not be used, sold, licenced, 
// transferred, copied or reproduced in whole or in part in 
// any manner or form or in or on any media to any person other than 
// in accordance with the terms of The Author's agreement
// or otherwise without the prior written consent of The Author.  All
// information contained in this source file is confidential information
// belonging to The Author and as such may not be disclosed other
// than in accordance with the terms of The Author's agreement, or
// otherwise, without the prior written consent of The Author.  As
// confidential information this source file must be kept fully and
// effectively secure at all times.
//


package com.cilogi.util.trie.compressed;

import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class CompressedTrie {
    static final Logger LOG = LoggerFactory.getLogger(CompressedTrie.class);

    private BitString data;
    private SimpleSelectIndex simpleSelect;

    private int letterStart;

    public CompressedTrie(String dataString, int nodeCount) {
        this.data = new BitString( dataString );

        // The position of the first bit of the data in 0th node. In non-root
        // nodes, this would contain 6-bit letters.
        this.letterStart = nodeCount * 2 + 1;
        this.simpleSelect = new SimpleSelectIndex(dataString, this.letterStart);
    }

    SimpleSelectIndex getSelectIndex() {
        return simpleSelect;
    }


    CompressedTrieNode getNodeByIndex(int index) {
        // retrieve the 6-bit letter.
        boolean isFinal = this.data.get( this.letterStart + index * 6, 1 ) == 1;
        int letterIndex =  data.get( this.letterStart + index * 6 + 1, 5);
        char letter = (char)(letterIndex + (int)'a');

        int sSel = this.simpleSelect.select(index + 1);
        int firstChild = sSel - index;

        // Since the nodes are in level order, this nodes children must go up
        // until the next node's children start.
        //int sel2 = this.directory.select( 0, index + 2 );
        int sSel2 =  this.simpleSelect.select(index + 2);
        int childOfNextNode = sSel2 - index - 1;

        return new CompressedTrieNode( this, index, letter, isFinal, firstChild, childOfNextNode - firstChild );
    }

    int getLetterStart() {
        return letterStart;
    }

    BitString getData() {
        return data;
    }


    CompressedTrieNode getRoot() {
        return this.getNodeByIndex( 0 );
    }

    public boolean lookup(String word) {
        CompressedTrieNode node = this.getRoot();
        for (int i = 0; i < word.length(); i++ ) {
            CompressedTrieNode child = null;
            int j = 0;
            int childCount = node.getChildCount();
            for (;j < childCount; j++) {
                child = node.getChild( j );
                if ( child.getLetter() == word.charAt(i) ) {
                    break;
                }
            }

            if ( j == node.getChildCount() ) {
                return false;
            }
            node = child;
        }

        return (node == null) ? false : node.isTerminal();
    }

    public JSONObject toJSON() {
        JSONObject json = new JSONObject();
        json.put("nodeCount", (letterStart-1)/2);
        json.put("directory", simpleSelect.getDirectory());
        json.put("trie", data.getData());
        return json;
    }
}
