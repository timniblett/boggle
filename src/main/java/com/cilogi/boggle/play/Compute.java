// Copyright (c) 2013 Cilogi. All Rights Reserved.
//
// File:        Compute.java  (06/08/13)
// Author:      tim
//
// Copyright in the whole and every part of this source file belongs to
// Cilogi (the Author) and may not be used, sold, licenced, 
// transferred, copied or reproduced in whole or in part in 
// any manner or form or in or on any media to any person other than 
// in accordance with the terms of The Author's agreement
// or otherwise without the prior written consent of The Author.  All
// information contained in this source file is confidential information
// belonging to The Author and as such may not be disclosed other
// than in accordance with the terms of The Author's agreement, or
// otherwise, without the prior written consent of The Author.  As
// confidential information this source file must be kept fully and
// effectively secure at all times.
//


package com.cilogi.boggle.play;

import com.cilogi.util.trie.Generate;
import com.cilogi.util.trie.TrieState;
import com.google.common.collect.Sets;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cilogi.util.trie.ITrie;

import java.util.Collections;
import java.util.Set;

public class Compute {
    static final Logger LOG = LoggerFactory.getLogger(Compute.class);

    private final ITrie trie;
    private final Board board;

    private final Set<String> words;

    public Compute(ITrie trie, Board board) {
        this.trie = trie;
        this.board = board;
        words = Sets.newTreeSet();
    }

    public void compute() {
        final int N_SQ = board.size();
        for (int i = 0; i < N_SQ; i++) {
            compute(i);
        }
    }

    private void compute(int startSquare) {
        State current = startState(startSquare);
        int location = current.getLocation();
        TrieState trieState = new TrieState(trie.getRoot());
        trieState = add(trieState, board.getValue(location));
        compute(current, trieState);
    }

    public Set<String> getWords() {
        return Collections.unmodifiableSet(words);
    }

    private void compute(State state, TrieState trieState) {
        if (trieState.isWord()) {
            words.add(trieState.getValue());
        }
        if (trieState.isPrefix()) {
            int[] neighbours = state.neighbours();
            for (int square : neighbours) {
                if (!state.hasVisited(square)) {
                    String val = board.getValue(square);
                    State newState = new State(state, square, val);
                    compute(newState, add(trieState, val));
                }
            }
        }
    }

    TrieState add(TrieState cur, String s) {
        for (int i = 0; i < s.length(); i++) {
            cur = cur.add(s.charAt(i));
        }
        return cur;
    }

    private State startState(int startSquare) {
        State state = new State();
        return new State(state, startSquare, board.getValue(0));
    }
}
