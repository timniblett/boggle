<!DOCTYPE html>
<html lang="en" >
<head>
<meta charset="utf-8">
<title>Offline Boggle Solver</title>
<meta name="description" content="Solve Boggle in an HTML5 site using the app cache to allow offline usage">
<meta name="HandheldFriendly" content="True">
<meta name="MobileOptimized" content="320">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="cleartype" content="on">

<link rel="apple-touch-icon-precomposed" sizes="144x144"
      href="images/touch/apple-touch-icon-144x144-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="114x114"
      href="images/touch/apple-touch-icon-114x114-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/touch/apple-touch-icon-72x72-precomposed.png">
<link rel="apple-touch-icon-precomposed" href="images/touch/apple-touch-icon-57x57-precomposed.png">
<link rel="shortcut icon" href="images/touch/apple-touch-icon.png">

<!-- Tile icon for Win8 (144x144 + tile color) -->
<meta name="msapplication-TileImage" content="images/touch/apple-touch-icon-144x144-precomposed.png">
<meta name="msapplication-TileColor" content="#222222">

<link rel="stylesheet" href="css/pure-0.2.1.min.css">
<link rel="stylesheet" href="css/baby-blue.css">
<link rel="stylesheet" href="css/main.css">
</head>
<body style="position:relative">
<div class="pure-g-r" id="layout">
<a href="#menu" id="menuLink" class="pure-menu-link">
    <span></span>
</a>

<div class="pure-u" id="menu">
    <div class="pure-menu pure-menu-open">
        <a class="pure-menu-heading" href="index.html">Boggle</a>
        <ul>
            <li class=" ">
                <a href="tech.html">Tech</a>
            </li>
            <li class=" ">
                <a href="about.html">About</a>
            </li>
         </ul>
    </div>
</div>    <div class="pure-u-1">
        <div class="hero">
            <h2>Technical Details</h2>
        </div>
        <div>
            <p>Developed by <a href="mailto:tim@timniblett.net">Tim Niblett</a></p>
<h2 id="motivation">Motivation</h2>
<p>Solving Boggle, client-side, in JavaScript, is an interesting challenge. If the solver is implemented as a web application then we would like the following characteristics:</p>
<ul>
<li>the amount of data uploaded should be small, for a fast start-up;</li>
<li>the time for code to initialize to be fast, again to enable a fast start-up;</li>
<li>and the run-time performance should be &quot;fast enough&quot; so that users aren't aware of waiting around for results.</li>
</ul>
<p>These are non-trivial requirements to implement in a webapp such as Boggle where a large dictionary is needed and a significant amount of computation takes place to &quot;solve&quot; a setup. The upside is that, if these requirements <em>can</em> be met, a large number of users can be supported at low cost as all the computation is done client-side. The other advantage of a client-side solution is that it can work offline, since nothing is required from the server in routine operation.</p>
<h3 id="succinct-tries">Succinct Tries</h3>
<p>John Resig wrote a series of blog posts (<a href="http://ejohn.org/blog/dictionary-lookups-in-javascript/">here</a>, <a href="http://ejohn.org/blog/javascript-trie-performance-analysis/">here</a> and <a href="http://ejohn.org/blog/revised-javascript-dictionary-search/">here</a>) a couple of years ago, studying a similar problem -- looking up words from a dictionary.</p>
<p>The most interesting solution Resig looked at was supplied by <a href="http://stevehanov.ca/blog/index.php?id=120">Steve Hanov</a> who used a Succinct data structure to implement a <a href="http://en.wikipedia.org/wiki/Trie">Trie</a>.</p>
<p>Succinct data structures were introduced by Jacobson <span class="citation">(G. J. Jacobson 1988)</span> in his PhD thesis (a summary paper can be found in <a href="http://www.cs.cmu.edu/afs/cs/project/aladdin/wwwlocal/compression/00063533.pdf"><span class="citation">(G. Jacobson 1989)</span></a>). They have interesting properties, being both space and time efficient. Succinct trees are asymptotically space-optimal yet can be used directly (without decompression or other pre-processing) and perform within a (small) constant time factor of pointer-based data structures.</p>
<p>This is very useful for our problem: the data structures can be sent from the server without taking up too much space, and the client does not need to decode the structures in order to process them. This is important for JavaScript as <a href="http://ejohn.org/blog/javascript-trie-performance-analysis/">Resig found</a> that decompressing a trie can take a long time on mobile devices (several seconds on an iPhone).</p>
<p>Hanov's solution, for which he provides <a href="http://www.hanovsolutions.com/trie/Bits.js">JavaScript</a> code, implements Jacobson's ideas with a twist. Bitstrings are stored in <a href="http://en.wikipedia.org/wiki/Base64">Base64</a>, which means the data doesn't need to be decoded client-side, at the expense of storing 6 bits of information in an 8-bit byte.</p>
<p>The only problem with Hanov's solution is that its slow. I downloaded a word list containing about <code>270,000</code> words from <a href="http://www.isc.ro/lists/twl06.zip">this file</a> <a href="http://www.isc.ro/en/commands/lists.html">here</a> and found that using Hanov's trie Boggle took about 20 seconds to solve on the HTC Desire, which is not acceptable.</p>
<p>For John Resig's problem, looking up a single word, Hanov's solution is probably OK as only a few nodes of the Trie have to be searched. The Boggle solver has to look at 3-5000 nodes of the trie as we're doing an exhaustive search of the Boggle board and this is too slow by at least an order of magnitude if we are to meet our run-time performance requirements.</p>
<p>The bottleneck occurs with the algorithm for the <code>select</code> function. <code>select(i)</code> finds the location in a bit string of the <code>i</code>th occurrence of the zero bit. Hanov's implementation is based on the <code>rank</code> function where <code>rank(i)</code> is the number of zero bits set up to and including position <code>i</code>. These two functions are inverses, so that if <code>rank(j) = k</code> then <code>select(k) = j</code>. Hanov's algorithm does a binary search over the <code>rank</code> function to determine select, and this is slow.</p>
<p>I have implemented a very simple-minded alternative <code>select</code> function which stores an array <code>S</code> where the <code>i</code>th element contains the value for <code>select(i * N)</code> where <code>N</code> is set at <code>32</code>, but can be any number. I've tried <code>64</code> which uses half the space and takes about 30% longer. To find <code>select(k)</code> we find <code>S[k/N]</code> and can then count the remaining bits. In effect each select is then looking for a value less than <code>N</code>. Performance depends on the distribution of the zeros and ones being reasonably uniform (which it is in this case). The speedup is significant -- in my tests a factor of about <code>14</code>. This makes the wait for a solution about a second on a slow phone, which is acceptable.</p>
<p>Having implemented this solution it looks very similar to that proposed, implemented and tested in <a href="http://www.dcc.uchile.cl/~gnavarro/ps/sea12.1.pdf"><span class="citation">(Navarro and Providel 2012)</span></a> for <code>select</code> queries.</p>
<hr />
<p>So, we end up with a data structure, which is reasonably compact. The gzipped trie (ie the size sent over the wire) is <code>368,640</code> bytes, while the gzipped dictionary is <code>470,298</code> bytes. The uncompressed dictionary is <em>much</em> larger than the uncompressed trie of course. So using the succinct structure saves space compared to the raw dictionary as well as being optimised for word search, and we can do everything in JavaScript with reasonable performance.</p>
<h3 id="offline-html">Offline HTML</h3>
<p>Setting up our web site/app to work offline is straightforward if we use the <a href="http://www.whatwg.org/specs/web-apps/current-work/multipage/offline.html">Offline cache</a> feature of HTML5 (well described <a href="http://diveintohtml5.info/offline.html">here</a>). The site has been implemented using the offline cache. One exception is that the online <a href="http://en.wiktionary.org/">Wiktionary</a> dictionary is used via <a href="http://www.wordnik.com/">Wordnik</a> to look up word meanings, and this only works offline -- an error message is provided of the service is unreachable.</p>
<h3 id="app-engine-service-and-source-code.">App Engine service and source code.</h3>
<p>In case others want to set up their own dictionaries we provide a simple service which allows a dictionary to be uploaded and a JavaScript form of the encoded dictionary to be loaded. This can be found <a href="http://solveboggle.appspot.com/service">here</a>. The JavaScript code for searching the Trie can be found in our Github repository <a href="http://timniblett.github.com/solveboggle">here</a></p>
<h2 id="references">References</h2>
<p>Jacobson, Guy. 1989. “Space-efficient static trees and graphs.” In <em>Foundations of Computer Science, 1989., 30th Annual Symposium on</em>, 549–554. IEEE.</p>
<p>Jacobson, Guy Joseph. 1988. “Succinct static data structures.” Pittsburgh, PA, USA.</p>
<p>Navarro, Gonzalo, and Eliana Providel. 2012. “Fast, small, simple rank/select on bitmaps.” In <em>Experimental Algorithms</em>, 295–306. Springer.</p>
        </div>
    </div>
</body>
<script src="http://code.jquery.com/jquery-1.10.1.min.js"></script>
<script>
    $("#menuLink").click(function(e) {
        $("#menuLink, #menu, #layout").toggleClass("active");
        e.preventDefault();
    })
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-27131467-7', 'solveboggle.appspot.com');
  ga('send', 'pageview');

</script>
</html>



