// Copyright (c) 2013 Cilogi. All Rights Reserved.
//
// File:        SimpleSelectIndex.java  (18/08/13)
// Author:      tim
//
// Copyright in the whole and every part of this source file belongs to
// Cilogi (the Author) and may not be used, sold, licenced, 
// transferred, copied or reproduced in whole or in part in 
// any manner or form or in or on any media to any person other than 
// in accordance with the terms of The Author's agreement
// or otherwise without the prior written consent of The Author.  All
// information contained in this source file is confidential information
// belonging to The Author and as such may not be disclosed other
// than in accordance with the terms of The Author's agreement, or
// otherwise, without the prior written consent of The Author.  As
// confidential information this source file must be kept fully and
// effectively secure at all times.
//


package com.cilogi.util.trie.compressed;

import com.google.common.collect.Lists;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;

// Find the i'th 0 in a bit string stored as Base64 characters
class SimpleSelectIndex {
    static final Logger LOG = LoggerFactory.getLogger(SimpleSelectIndex.class);

    private static final int START_VAL = -1;

    private static final int CHUNK_SIZE = 32;
    private final static int WIDTH = Base64.width();

    private StringIntArray directory;
    private final String data;

    SimpleSelectIndex(String data) {
        this(data, data.length() * WIDTH);
    }

    SimpleSelectIndex(String data, int nBits) {
        directory = new StringIntArray();
        this.data = data;
        int nChars = 1 + nBits/WIDTH;
        init((nChars < this.data.length()) ? nChars : this.data.length());
    }

    String getData() {
        return data;
    }

    String getDirectory() {
        return directory.toString();
    }

    int size() {
        return directory.size();
    }

    int select(int index) {
        State state = new State(index, data, directory);
        while (state.getCount() < index) {
            state.step();
        }
        return state.getIndex();
    }

    private void init(int nChars) {
        int curCount = 0;
        int index = 0;
        int totalZero = 0;
        int totalBits = 0;
        for (int i = 0; i < nChars; i++) {
            char c = data.charAt(i);
            int nZeroBits = WIDTH - Base64.bits(c);
            totalZero += nZeroBits;
            totalBits += WIDTH;
            int nextCount = curCount + nZeroBits;
            if (nextCount < CHUNK_SIZE) {
                index = totalBits;
                curCount = nextCount;
            }  else {
                assert nextCount >= CHUNK_SIZE;
                int nBitsToComplete =  CHUNK_SIZE - curCount;
                int charIndex  = findZeroBit(nBitsToComplete, c);
                index += charIndex;
                directory.add(index);
                curCount = nZeroBits - nBitsToComplete;
                index = totalBits;
            }
        }
        LOG.debug("TOTAL number of zero bits is: " + totalZero);
        LOG.debug("Directory is " + directory.size() + " entries");
    }

    // find the i'th 0 bit  in a character, counting bits from the left and assuming that there are enough bits
    // i can be 0, in which case the result is 0;
    private int findZeroBit(int i, char c) {
        if (i == 0) {
            return 0;
        }
        int toFind = i;
        int cVal = Base64.value(c);
        for (int j = WIDTH - 1; j >= 0; j--) {
            int val = (1<<j) & cVal;
            if (val == 0) {
                toFind--;
                if (toFind == 0) {
                    return WIDTH - 1 - j;
                }
            }
        }
        assert false : "Shouldn't be able to get here";
        return START_VAL;
    }

    private static class State {
        int targetCount;
        String data;
        int currentCount;

        int currentOffset;
        int currentChar;

        State(int bitCount, String data, StringIntArray directory) {
            this.targetCount = bitCount;
            this.data = data;
            int chunk = (bitCount / CHUNK_SIZE) - 1;
            if (chunk >= directory.size()) {
                LOG.debug("oops");
            }
            int startCount = (chunk == START_VAL) ? 0 : (1 + chunk) * CHUNK_SIZE;
            int startPosition = (chunk == START_VAL) ? -1 : directory.get(chunk);

            currentCount = startCount;

            // currentChar/currentOffset point to next bit beyond currentCount except when targetCount is reached
            currentChar = (startPosition == START_VAL) ? 0 : startPosition/WIDTH;
            currentOffset = (startPosition == START_VAL) ? -1 : startPosition % WIDTH;
            if (currentCount < bitCount) {
                advance();
            }
        }

        void advance() {
            currentOffset++;
            if (currentOffset > 0 && currentOffset % WIDTH == 0) {
                currentChar++;
                currentOffset = 0;
            }
        }

        int getIndex() {
            return (currentCount == 0) ? -1 : currentChar * WIDTH + currentOffset;
        }

        int getCount() {
            return currentCount;
        }


        int step() {
            if (currentOffset > 0) {
                int val = val();
                if (val == 0) {
                    currentCount++;
                }
                if (currentCount < targetCount) {
                    advance();
                }
            } else {
                assert currentOffset == 0;
                int bits = WIDTH - Base64.bits(data.charAt(currentChar));
                if (currentCount + bits < targetCount) {
                    currentCount += bits;
                    currentChar++;
                    currentOffset = 0;
                } else {
                    int val = val();
                    if (val == 0) {
                        currentCount++;
                    }
                    if (currentCount < targetCount) {
                        currentOffset++;
                    }
                }
            }
            return currentCount;
        }

        int val() {
            int value = Base64.value(data.charAt(currentChar));
            int offset = WIDTH-1-currentOffset;
            return  (1<<offset) & value;
        }
    }
}
