define(['jquery', 'dict', 'trie/compressedtrie', "boggle/bogglesolver", "vendor/mustache", "text!templates/wordList.txt",
    "meaning", "vendor/fastclick", "boardchars", "caret"],
        function ($, dict, CompressedTrie, Solver, Mustache, wordList, meaning, fastClick, boardChars) {

            return function () {

                var UNKNOWNS = [
                    "I'm stumped on this one",
                    "Search me",
                    "Words fail me",
                    "Clueless in Boggle-land",
                    "O brave new world that has such words in it",
                    "Have you tried lexotherapy?"
                ];

                jQuery.fn.center = function () {
                    this.css("position", "absolute");
                    this.css("top", Math.max(0, (($(window).height() - $(this).outerHeight()) / 2) +
                            $(window).scrollTop()) + "px");
                    this.css("left", Math.max(0, (($(window).width() - $(this).outerWidth()) / 2) +
                            $(window).scrollLeft()) + "px");
                    return this;
                }

                fastClick.attach(document.body);


                var trie = new CompressedTrie(dict.trie, dict.nodeCount, dict.simpleDirectory);

                setReady();

                $("#solve-button").click(function () {
                    $("body").addClass("loading");
                    setTimeout(findAndSetWords, 0);
                });

                $("#clear-button").click(function () {
                    clear();
                    setReady();
                });


                $("#word-list").on('click', '.dict', function (e) {
                    var term = $.trim($(this).text());
                    $("#dict").hide();
                    $("body").addClass("loading");
                    setTimeout(function () {
                        loadDict(term);
                    }, 0);
                });

                $("#close-dict").click(function () {
                    $("#dict").hide(200);
                });

                boardChars.setup($("#in"), setReady);

                function loadDict(term) {
                    meaning(term, {
                        success: function (data) {
                            var theMeaning = decode(data);
                            $("#dict-content").html("<p>" + theMeaning + "</p>");
                            $("#dict").center().show();
                            $("body").removeClass("loading");
                        },
                        error: function () {
                            $("#dict-content").html("<p style='color:red'>Can't access the dictionary, offline perhaps?</p>");
                            $("#dict").center().show();
                            $("body").removeClass("loading");
                        }
                    });
                }

                function decode(data) {
                    var index, primary;

                    if (data.length) {
                        primary = data[0];
                        return "(" + primary.partOfSpeech + ") " + primary.text;
                    } else {
                        index = Math.floor(Math.random() * UNKNOWNS.length);
                        return UNKNOWNS[index];
                    }
                }

                function setReady() {
                    var ready = isReady();
                    if (ready) {
                        $("#solve-button").removeAttr("disabled");
                    } else {
                        $("#solve-button").attr("disabled", "disabled");
                    }
                    //console.log("solve set to " + ready);
                }

                function isReady() {
                    var val;
                    for (var i = 0; i < 16; i++) {
                        val = $("#cell" + i).val();
                        if (!val) {
                            return false;
                        }
                    }
                    return true;
                }

                function clear() {
                    $("#word-list").html("");
                    for (var i = 0; i < 16; i++) {
                        $("#cell" + i).val("");
                    }
                    $("#cell0").focus();
                    boardChars.clear();
                    setReady();
                }

                function currentBoard() {
                    var i, val, out = [];
                    for (i = 0; i < 16; i++) {
                        val = $("#cell" + i).val();
                        out.push((val == "q") ? "qu" : val);
                    }
                    return out;
                }

                function solve() {
                    var letters = currentBoard();
                    var solver = new Solver(trie, letters);
                    solver.solve();
                    //console.log("# nodes: " + solver.count);
                    return solver.wordsByLength;
                }

                function findAndSetWords() {
                    var i, html = "", words, len, score, wordsByLength = solve();

                    for (i = 0; i < wordsByLength.length; i++) {
                        words = wordsByLength[i];
                        len = words[0].length;
                        score = boggleScore(len);
                        html += Mustache.render(wordList, {
                            len: len,
                            score: score,
                            words: words
                        });
                    }
                    $("#word-list").html(html);
                    setTimeout(function () {
                        $("body").removeClass("loading");
                    }, 0);

                }

                function boggleScore(len) {
                    if (len < 3) {
                        return 0;
                    } else if (len == 3 || len == 4) {
                        return  1;
                    } else if (len == 5) {
                        return 2;
                    } else if (len == 6) {
                        return 3;
                    } else if (len == 7) {
                        return 5;
                    } else {
                        return 11;
                    }
                }

                $("#toggleModal").click(function () {
                    $("#modalDict").modal("toggle");
                })

            };
        });