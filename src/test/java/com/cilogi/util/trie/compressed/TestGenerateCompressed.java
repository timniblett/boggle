// Copyright (c) 2013 Cilogi. All Rights Reserved.
//
// File:        TestGenerateCompressed.java  (13/08/13)
// Author:      tim
//
// Copyright in the whole and every part of this source file belongs to
// Cilogi (the Author) and may not be used, sold, licenced, 
// transferred, copied or reproduced in whole or in part in 
// any manner or form or in or on any media to any person other than 
// in accordance with the terms of The Author's agreement
// or otherwise without the prior written consent of The Author.  All
// information contained in this source file is confidential information
// belonging to The Author and as such may not be disclosed other
// than in accordance with the terms of The Author's agreement, or
// otherwise, without the prior written consent of The Author.  As
// confidential information this source file must be kept fully and
// effectively secure at all times.
//


package com.cilogi.util.trie.compressed;

import com.cilogi.util.trie.Generate;
import com.cilogi.util.trie.Trie;

import com.google.common.collect.Lists;
import org.junit.Before;
import org.junit.Test;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

import static org.junit.Assert.*;

public class TestGenerateCompressed {
    static final Logger LOG = LoggerFactory.getLogger(TestGenerateCompressed.class);


    public TestGenerateCompressed() {
    }

    @Before
    public void setUp() {

    }

    @Test
    public void testSimple() {
        Trie trie = createTrie();
        GenerateCompressed gen = new GenerateCompressed();
        String output = gen.compressString(trie);
        //LOG.debug("output = " + output);
        assertEquals("{\"directory\":\"\",\"trie\":\"tapJ-cmaOtIjcJI\",\"nodeCount\":11}", output);
    }

    @Test
    public void testLookup() {
        Trie trie = createTrie();
        String trieData = trie.encode();

        CompressedTrie frozenTrie = new CompressedTrie(trieData, trie.getNodeCount());
        assertTrue(frozenTrie.lookup("one"));
        assertTrue(frozenTrie.lookup("two"));
        assertTrue(frozenTrie.lookup("three"));
        assertFalse(frozenTrie.lookup("four"));
    }

    private Trie createTrie() {
        List<String> words = Lists.newArrayList("one", "three", "two");
        GenerateCompressed gen = new GenerateCompressed();
        Trie trie = gen.generateTrie(words);
        String trieData = trie.encode();
        return trie;
    }

    @Test
    public void testFullDict() {
        Trie trie = Generate.trie();
        String trieData = trie.encode();
        CompressedTrie frozenTrie = new CompressedTrie(trieData, trie.getNodeCount());
        List<String> words = Generate.wordList();
        int count = 0;
        long start = System.currentTimeMillis();


        //frozenTrie.compare(31, 7000000);

        //SimpleSelectIndex ss = frozenTrie.getSelectIndex();
        //int bitIndex = ss.select(32);

        frozenTrie.lookup("two");
        for (String word : words) {
            count++;
            try {
                assertTrue("Can't look up " + word + " count " + count, frozenTrie.lookup(word.toLowerCase()));
            } catch (Throwable e) {
                LOG.error("Can't look up " + word + " count " + count);
                throw e;
            }
            //assertTrue("Can't look up " + word + " count " + count, trie.hasWord(word));
        }

        LOG.debug("time " + (System.currentTimeMillis() - start) + "ms");
        System.out.println(frozenTrie.toJSON().toJSONString());
    }
}