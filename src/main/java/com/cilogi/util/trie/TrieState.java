// Copyright (c) 2013 Cilogi. All Rights Reserved.
//
// File:        TrieState.java  (06/08/13)
// Author:      tim
//
// Copyright in the whole and every part of this source file belongs to
// Cilogi (the Author) and may not be used, sold, licenced, 
// transferred, copied or reproduced in whole or in part in 
// any manner or form or in or on any media to any person other than 
// in accordance with the terms of The Author's agreement
// or otherwise without the prior written consent of The Author.  All
// information contained in this source file is confidential information
// belonging to The Author and as such may not be disclosed other
// than in accordance with the terms of The Author's agreement, or
// otherwise, without the prior written consent of The Author.  As
// confidential information this source file must be kept fully and
// effectively secure at all times.
//


package com.cilogi.util.trie;

import lombok.NonNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class TrieState  {
    static final Logger LOG = LoggerFactory.getLogger(TrieState.class);

    private final ITrieNode node;
    private final String prefix;

    public TrieState(@NonNull ITrieNode node) {
        this(node, "");
    }

    private TrieState(ITrieNode node, String prefix) {
        this.node = node;
        this.prefix = prefix;
    }

    public TrieState add(char c) {
        if (node != null) {
            ITrieNode next = node.find(c);
            return new TrieState(next, prefix + c);
        } else {
            return this;
        }
    }

    public boolean isPrefix() {
        return "".equals(prefix) ? true : node != null;
    }

    public boolean isWord() {
        return node != null && node.isTerminal();
    }

    public String getValue() {
        return prefix;
    }

}
