// Copyright (c) 2010 Tim Niblett All Rights Reserved.
//
// File:        RouteModule.java  (05-Oct-2010)
// Author:      tim

//
// Copyright in the whole and every part of this source file belongs to
// Tim Niblett (the Author) and may not be used,
// sold, licenced, transferred, copied or reproduced in whole or in
// part in any manner or form or in or on any media to any person
// other than in accordance with the terms of The Author's agreement
// or otherwise without the prior written consent of The Author.  All
// information contained in this source file is confidential information
// belonging to The Author and as such may not be disclosed other
// than in accordance with the terms of The Author's agreement, or
// otherwise, without the prior written consent of The Author.  As
// confidential information this source file must be kept fully and
// effectively secure at all times.
//


package com.cilogi.boggle.web.guice;


import java.util.Map;
import java.util.logging.Logger;

import com.cilogi.boggle.web.servlet.CreateDictServlet;
import com.cilogi.boggle.web.servlet.DownloadDictServlet;
import com.cilogi.boggle.web.servlet.WakeServlet;
import com.google.appengine.tools.appstats.AppstatsFilter;
import com.google.appengine.tools.appstats.AppstatsServlet;
import com.google.common.collect.Maps;
import com.google.inject.servlet.ServletModule;
import com.google.common.base.Preconditions;

public class RouteModule extends ServletModule {
    static final Logger LOG = Logger.getLogger(RouteModule.class.getName());


    public RouteModule() {}

    @Override
    protected void configureServlets() {
        filter("/*").through(AppstatsFilter.class);
        serve("/appstats/*").with(AppstatsServlet.class);
        serve("/create").with(CreateDictServlet.class);
        serve("/download").with(DownloadDictServlet.class);
        serve("/cron/wake").with(WakeServlet.class);
    }

    private static Map<String,String> map(String... params) {
        Preconditions.checkArgument(params.length % 2 == 0, "You have to have a n even number of map params");
        Map<String,String> map = Maps.newHashMap();
        for (int i = 0; i < params.length; i+=2) {
            map.put(params[i], params[i+1]);
        }
        return map;
    }
}
