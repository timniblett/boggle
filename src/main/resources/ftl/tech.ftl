<!DOCTYPE html>
<html lang="en" ${manifest}>
<head>
<#include "inc/head.ftl">
</head>
<body style="position:relative">
<div class="pure-g-r" id="layout">
<#include "menu.ftl">
    <div class="pure-u-1">
        <div class="hero">
            <h2>Technical Details</h2>
        </div>
        <div>
            <p>Developed by <a href="mailto:tim@timniblett.net">Tim Niblett</a></p>
            <#include "inc/tech-include.ftl">
        </div>
    </div>
</body>
<#include "inc/foot.ftl">

</html>



