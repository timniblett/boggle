({
    baseUrl: "js/",
    out: "js/main-built.js",
    optimize : "uglify2",
    name: "main",
    preserveLicenseComments: false,
    mainConfigFile: 'js/main.js'
})