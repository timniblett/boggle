define([], function() {
    var ADJACENT = [
                [1, 4, 5],
                [2, 6, 5, 4, 0],
                [3, 7, 6, 5, 1],
                [7, 6, 2],

                [0, 1, 5, 9, 8],
                [1, 2, 6, 10, 9, 8, 4, 0],
                [2, 3, 7, 11, 10, 9, 5, 1],
                [3, 11, 10, 6, 2],

                [4, 5, 9, 13, 12],
                [5, 6, 10, 14, 13, 12, 8, 4],
                [6, 7, 11, 15, 14, 13, 9, 5],
                [7, 15, 14, 10, 6],

                [8, 9, 13],
                [9, 10, 14, 12, 8],
                [10, 11, 15, 13, 9],
                [11, 14, 10]
        ];

    function Board(values) {
        this.values = values;
    }



    Board.prototype.getValue = function(location) {
        return this.values[location];
    }

    Board.prototype.size = function() {
        return ADJACENT.length;
    }

    Board.adjacent = function(location) {
        return ADJACENT[location];
    }

    Board.size = 16;

    return Board;
});