// Copyright (c) 2013 Cilogi. All Rights Reserved.
//
// File:        State.java  (06/08/13)
// Author:      tim
//
// Copyright in the whole and every part of this source file belongs to
// Cilogi (the Author) and may not be used, sold, licenced, 
// transferred, copied or reproduced in whole or in part in 
// any manner or form or in or on any media to any person other than 
// in accordance with the terms of The Author's agreement
// or otherwise without the prior written consent of The Author.  All
// information contained in this source file is confidential information
// belonging to The Author and as such may not be disclosed other
// than in accordance with the terms of The Author's agreement, or
// otherwise, without the prior written consent of The Author.  As
// confidential information this source file must be kept fully and
// effectively secure at all times.
//


package com.cilogi.boggle.play;

import com.google.common.base.Preconditions;
import lombok.Getter;
import lombok.Setter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.BitSet;

/** The search state */
public class State {
    static final Logger LOG = LoggerFactory.getLogger(State.class);

    /*
     * The squares of the borad are labelled as
     * <pre>
     *  0  1  2  3
     *  4  5  6  7
     *  8  9 10 11
     * 12 13 14 15
     * </pre>
     */

    private BitSet visited;

    @Setter
    @Getter
    private int location;

    @Getter
    private String prefix;

    private Board board;

    public State() {
        visited = new BitSet();
        prefix = "";
        this.location = -1;
        this.board = new Board();
    }

    public State(State prev, int location, String chars) {
        this();
        Preconditions.checkArgument(location >= 0 && location < board.size());

        if (prev.hasVisited(location)) {
            throw new RuntimeException("Can't visit previous site");
        }
        this.prefix = prev.prefix + chars;
        this.visited = (BitSet)prev.visited.clone();
        this.visited.set(location);
        this.location = location;
    }

    public boolean hasVisited(int location) {
        return visited.get(location);
    }

    public int[] neighbours() {
        return (location == -1) ? new int[0] : board.adjacent(location);
    }
}
