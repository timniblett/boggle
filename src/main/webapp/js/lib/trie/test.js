// do a quick test on the trie
define(['dict', 'trie/compressedtrie', "log"], function(dict, CompressedTrie, log) {

    return function() {
        var trie = new CompressedTrie( dict.trie, dict.nodeCount, dict.simpleDirectory);
        var words = [
                "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten"
        ];
        var word, val;
        for (var i = 0; i < words.length; i++) {
            word = words[i];
            val = trie.lookup(word);
            log("val for " + word + " is " + val);
        }
    }
});