// Copyright (c) 2013 Tim Niblett All Rights Reserved.
//
// File:        BindingModule.java
// Author:      tim

//
// Copyright in the whole and every part of this source file belongs to
// Tim Niblett (the Author) and may not be used,
// sold, licenced, transferred, copied or reproduced in whole or in
// part in any manner or form or in or on any media to any person
// other than in accordance with the terms of The Author's agreement
// or otherwise without the prior written consent of The Author.  All
// information contained in this source file is confidential information
// belonging to The Author and as such may not be disclosed other
// than in accordance with the terms of The Author's agreement, or
// otherwise, without the prior written consent of The Author.  As
// confidential information this source file must be kept fully and
// effectively secure at all times.
//


package com.cilogi.boggle.web.guice;

import com.cilogi.boggle.web.util.CreateDoc;
import com.google.appengine.api.utils.SystemProperty;
import com.google.appengine.tools.appstats.AppstatsFilter;
import com.google.appengine.tools.appstats.AppstatsServlet;
import com.google.common.base.Charsets;
import com.google.inject.AbstractModule;
import com.google.inject.Scopes;
import com.google.inject.name.Names;
import freemarker.template.Configuration;
import freemarker.template.TemplateModelException;

import java.io.IOException;
import java.net.URL;
import java.util.Locale;
import java.util.logging.Logger;


public class BindingModule extends AbstractModule {
    static final Logger LOG = Logger.getLogger(BindingModule.class.getName());


    public BindingModule() {}

    @Override
    protected void configure() {
        //bindConstant().annotatedWith(Names.named("registrationExpiryHours")).to(12L);
        bindString("host", isDevelopmentServer() ? "http://localhost:8080" : "http://bogglesolver.appspot.com:80");
    }

    private void bindString(String key, String value) {
        bind(String.class).annotatedWith(Names.named(key)).toInstance(value);
        bind(CreateDoc.class).toInstance(createDoc());
        bind(AppstatsFilter.class).in(Scopes.SINGLETON);
        bind(AppstatsServlet.class).in(Scopes.SINGLETON);
    }



    private static boolean isDevelopmentServer() {
        SystemProperty.Environment.Value server = SystemProperty.environment.value();
        return server == SystemProperty.Environment.Value.Development;
    }

    private CreateDoc createDoc() {
        try {
            URL base = getClass().getResource("/ftl/");
            CreateDoc create = new CreateDoc(base, Locale.getDefault(), Charsets.UTF_8.name());
            Configuration cfg = create.cfg();
            cfg.setSharedVariable("dev", true);
            return create;
        } catch (IOException e) {
            throw new RuntimeException(e);
        } catch (TemplateModelException e) {
            throw new RuntimeException(e);
        }
    }
}
