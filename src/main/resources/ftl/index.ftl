<!DOCTYPE html>
<html lang="en" ${manifest}>
<head>
    <#include "inc/head.ftl">
</head>
<body style="position:relative">
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=429173527191857";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
</script>

    <div class="pure-g-r" id="layout">
        <#include "menu.ftl">
        <div class="pure-u-1">
            <div class="hero">
                <h1>Boggle Solver</h1>
            </div>

            <div id="instructions">
                <p>
                    Fill in the letters for the Boggle puzzle you wish to solve,
                    then click <b>Solve</b> or hit Enter. A 'q' will be treated as
                    'qu', following Boggle's convention. Its faster to type the 16
                    characters into the text field above the buttons, especially on mobile
                </p>
                <!--
                <p style="letter-spacing:1em; width: 7em; font-family: courier;">abcd efgh ijkl</p>
                -->
            </div>
            <div id="boggle-area">
                <div id="boggle-panel">
                    <div id="board" class="boggle-board">
                        <div class="row">
                            <input type="text" id="cell0" class="cell" maxlength="1" data-index="0" value="w">
                            <input type="text" id="cell1" class="cell" maxlength="1" data-index="1" value="e">
                            <input type="text" id="cell2" class="cell" maxlength="1" data-index="2" value="l">
                            <input type="text" id="cell3" class="cell" maxlength="1" data-index="3" value="c">
                        </div>
                        <div class="row">
                            <input type="text" id="cell4" class="cell" maxlength="1" data-index="4" value="o">
                            <input type="text" id="cell5" class="cell" maxlength="1" data-index="5" value="m">
                            <input type="text" id="cell6" class="cell" maxlength="1" data-index="6" value="e">
                            <input type="text" id="cell7" class="cell" maxlength="1" data-index="7" value="t">
                        </div>
                        <div class="row">
                            <input type="text" id="cell8" class="cell" maxlength="1" data-index="8" value="o">
                            <input type="text" id="cell9" class="cell" maxlength="1" data-index="9" value="b">
                            <input type="text" id="cell10" class="cell" maxlength="1" data-index="10" value="o">
                            <input type="text" id="cell11" class="cell" maxlength="1" data-index="11" value="g">
                        </div>
                        <div class="row">
                            <input type="text" id="cell12" class="cell" maxlength="1" data-index="12" value="g">
                            <input type="text" id="cell13" class="cell" maxlength="1" data-index="13" value="l">
                            <input type="text" id="cell14" class="cell" maxlength="1" data-index="14" value="e">
                            <input type="text" id="cell15" class="cell" maxlength="1" data-index="15" value="s">
                        </div>
                    </div>
                </div>
                <form class="pure-form" style="margin-top:1em">
                    <input id="in" type="text" width="16" style="width:11.2em" value="welcometoboggles">
                </form>
                <div id="controls">
                    <button id="solve-button" class="pure-button pure-button-primary" disabled>Solve</button>
                    <button id="clear-button" class="pure-button">Clear</button>
                </div>
                <div>
                    <div id="fb-like" class="fb-like" data-href="http://home.cilogi.com/boggle"
                         data-width="320" data-show-faces="true" data-send="true"></div>
                </div>
            </div>
            <div id="words-panel" style="display:none">
                <p id="num-words"></p>

                <p id="solving-message" style="display:none">Solving puzzle...</p>
            </div>
            <div id="word-list"></div>
        </div>
    </div>
    <div class="modal"></div>
    <#include "modal.ftl">
</body>

<#include "inc/foot.ftl">

<#if develop??>
<script data-main="js/main.js" src="js/lib/require.js"></script>
<#else>
<script src="js/lib/require.js"></script>
<script src="js/main-built.js"></script>
</#if>
</html>



