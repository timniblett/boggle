// Copyright (c) 2013 Cilogi. All Rights Reserved.
//
// File:        CompressedTrieNode.java  (12/08/13)
// Author:      tim
//
// Copyright in the whole and every part of this source file belongs to
// Cilogi (the Author) and may not be used, sold, licenced, 
// transferred, copied or reproduced in whole or in part in 
// any manner or form or in or on any media to any person other than 
// in accordance with the terms of The Author's agreement
// or otherwise without the prior written consent of The Author.  All
// information contained in this source file is confidential information
// belonging to The Author and as such may not be disclosed other
// than in accordance with the terms of The Author's agreement, or
// otherwise, without the prior written consent of The Author.  As
// confidential information this source file must be kept fully and
// effectively secure at all times.
//


package com.cilogi.util.trie.compressed;

import com.cilogi.util.trie.ITrieNode;
import lombok.Getter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class CompressedTrieNode implements ITrieNode {
    static final Logger LOG = LoggerFactory.getLogger(CompressedTrieNode.class);

    private CompressedTrie trie;
    private int index;

    @Getter
    private char letter;

    @Getter
    private boolean terminal;

    private int firstChild;
    private int childCount;

    public CompressedTrieNode(CompressedTrie trie, int index, char letter, boolean isFinal, int firstChild, int childCount) {
        this.trie = trie;
        this.index = index;
        this.letter = letter;
        this.terminal = isFinal;
        this.firstChild = firstChild;
        this.childCount = childCount;
    }

    @Override
    public ITrieNode find(char c) {
        return getChild((int)(c - 'a'));
    }


    int getChildCount() {
        return childCount;
    }

    CompressedTrieNode getChild(int index) {
        return this.trie.getNodeByIndex(firstChild + index);
    }

    @Override
    public String toString() {
        return "(frozenTrieNode index " + index + " letter " + letter + " isFinal " + terminal + " firstChild " + firstChild + " childCount " + childCount + ")";
    }
}
