define("jquery", [], function() {
    return jQuery;
});

require.config({
    baseUrl: "js",
    catchError : {
        define: true
    },
    // 3rd party script alias names
    paths: {
        "vendor" : "lib/vendor",
        "underscore" : "lib/vendor/underscore-1.5.1",
        "caret": "lib/vendor/jquery.caret.1.02",
        "trie" : "lib/trie",
        "boggle" : "lib/boggle"
    },

    // Sets the configuration for your third party scripts that are not AMD compatible
    shim: {
        'underscore' : {
                    exports : '_'
        },
        "caret" : ['jquery'],
        "modal" : ['jquery']
    }

});


require(['jquery', 'boggle-app'],
        function ($, appFun) {

    $(document).ready(appFun);
});
