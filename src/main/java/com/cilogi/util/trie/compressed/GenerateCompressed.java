// Copyright (c) 2013 Cilogi. All Rights Reserved.
//
// File:        GenerateCompressed.java  (13/08/13)
// Author:      tim
//
// Copyright in the whole and every part of this source file belongs to
// Cilogi (the Author) and may not be used, sold, licenced, 
// transferred, copied or reproduced in whole or in part in 
// any manner or form or in or on any media to any person other than 
// in accordance with the terms of The Author's agreement
// or otherwise without the prior written consent of The Author.  All
// information contained in this source file is confidential information
// belonging to The Author and as such may not be disclosed other
// than in accordance with the terms of The Author's agreement, or
// otherwise, without the prior written consent of The Author.  As
// confidential information this source file must be kept fully and
// effectively secure at all times.
//


package com.cilogi.util.trie.compressed;

import com.cilogi.util.trie.Trie;

import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Pattern;


public class GenerateCompressed {
    static final Logger LOG = LoggerFactory.getLogger(GenerateCompressed.class);

    private static final Pattern PATTERN = Pattern.compile("^[a-z]+$");

    public GenerateCompressed() {

    }

    public CompressedTrie compress(Trie trie) {
        String trieData = trie.encode();
        CompressedTrie compressedTrie = new CompressedTrie(trieData, trie.getNodeCount());
        return compressedTrie;
    }

    public JSONObject compressJSON(Trie trie) {
        return compress(trie).toJSON();
    }

    public String compressString(Trie trie) {
        return compressJSON(trie).toJSONString();
    }

    public Trie generateTrie(List<String> inWords) {
        Trie trie = new Trie();
        List<String> words = copy(inWords);
        for (String word : words) {
            trie.addWord(word);
        }
        return trie;
    }

    static List<String> copy(List<String> words) {
        List<String> out = new ArrayList<String>(words.size());
        for (String word : words) {
            String lc = word.toLowerCase();
            if (isLower(lc)) {
                out.add(lc);
            }
        }
        Collections.sort(out);
        return out;
    }

    static boolean isLower(String word) {
        return PATTERN.matcher(word).matches();
    }

}
