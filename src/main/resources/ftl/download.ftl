<!DOCTYPE html>
<html lang="en">
<head>
    <#include "inc/head.ftl">
</head>
<body style="position:relative">
    <div class="pure-g-r" id="layout">
        <#include "menu.ftl">
        <div class="pure-u-1">
            <div class="hero">
                <h2>Dictionary has been created</h2>
            </div>

            <div>
                <p>Your dictionary has been created.</p>
                <form class="pure-form" method="POST" action="/download?key=${key}&sz=${sz}" enctype="multipart/form-data">
                    <legend>Download your dictionary</legend>
                    <fieldset>
                        <button type="submit" class="pure-button pure-button-primary">Download</button>
                    </fieldset>
                </form>
            </div>
    </div>
</body>
<#include "inc/foot.ftl">

</html>



