// Copyright (c) 2013 Cilogi. All Rights Reserved.
//
// File:        TestGenerate.java  (06/08/13)
// Author:      tim
//
// Copyright in the whole and every part of this source file belongs to
// Cilogi (the Author) and may not be used, sold, licenced, 
// transferred, copied or reproduced in whole or in part in 
// any manner or form or in or on any media to any person other than 
// in accordance with the terms of The Author's agreement
// or otherwise without the prior written consent of The Author.  All
// information contained in this source file is confidential information
// belonging to The Author and as such may not be disclosed other
// than in accordance with the terms of The Author's agreement, or
// otherwise, without the prior written consent of The Author.  As
// confidential information this source file must be kept fully and
// effectively secure at all times.
//


package com.cilogi.util.trie;

import org.junit.Before;
import org.junit.Test;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

import static org.junit.Assert.*;

public class TestGenerate {
    static final Logger LOG = LoggerFactory.getLogger(TestGenerate.class);


    public TestGenerate() {
    }

    @Before
    public void setUp() {

    }

    @Test
    public void testGetWords() {
        String s = "The cat sat on the mat\r\n";
        Generate gen = new Generate(s);
        List<String> words = gen.words();
        assertEquals(6, words.size());
        assertEquals("The", words.get(0));
        assertEquals("sat", words.get(2));
        assertEquals("mat", words.get(5));
    }

    @Test
    public void testGetWordsInLines() {
        String s = "The\r\ncat\r\nsat\non\nthe\nmat\r\n";
        Generate gen = new Generate(s);
        List<String> words = gen.words();
        assertEquals(6, words.size());
        assertEquals("The", words.get(0));
        assertEquals("sat", words.get(2));
        assertEquals("mat", words.get(5));
    }
    @Test
    public void testReturnIsSpace() {
        assertFalse(Character.isLetter('\r'));
        assertFalse(Character.isLetter(' '));
        assertFalse(Character.isLetter('\n'));
    }

    @Test
    public void testGenerate() {
        String s = "The cat sat on the mat\r\n";
        try {
            Trie trie = new Generate(s).generate().getTrie();
            assertTrue(trie.hasWord("the"));
        } catch (Exception e) {
            fail("Can't generate trie: " + e.getMessage());
        }
    }

    @Test
    public void testTrie() {
        try {
            Trie trie = Generate.trie();
            assertTrue(trie.hasWord("the"));
        } catch (RuntimeException e) {
            fail("Oops " + e.getMessage());
        }
    }
}