<!DOCTYPE html>
<html lang="en" ${manifest}>
<head>
    <#include "inc/head.ftl">
</head>
<body style="position:relative">
    <div class="pure-g-r" id="layout">
        <#include "menu.ftl">
        <div class="pure-u-1">
            <div class="hero">
                <h2>About Boggle Solver</h2>
            </div>

            <div>
                <p>Developed by <a href="mailto:tim@timniblett.net">Tim Niblett</a></p>
                <p>I originally developed it for my niece who always beats me at Boggle,
                   but who may not always find the longest words.</p>
                <p>The interest was in seeing if a reasonably fast word search could be programmed
                   in a pure JavaScript web app (see <a href="tech.html">tech</a>).  The result is just about usable on an HTC Desire,
                   which is a 2009 era smart-phone with poor JavaScript performance.  On an HTC One X,
                   from 2012 the performance is sub-second.</p>
            </div>
    </div>
</body>
<#include "inc/foot.ftl">
</html>



