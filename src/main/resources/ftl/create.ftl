<!DOCTYPE html>
<html lang="en">
<head>
    <#include "inc/head.ftl">
</head>
<body style="position:relative">
    <div class="pure-g-r" id="layout">
        <#include "menu.ftl">
        <div class="pure-u-1">
            <div class="hero">
                <h2>Create a dictionary</h2>
            </div>

            <div>
                <p>Create a JavaScript dictionary which you can use to lookup words client-side.</p>
                <p>The uploaded file must contain words separated by spaces and/or on separate lines</p>
                <form class="pure-form" method="POST" action="${url}" enctype="multipart/form-data">
                    <legend>Upload your dictionary</legend>
                    <fieldset>
                        <label for="dictionary">
                            <input id="dictionary" name="dictionary" type="file">
                        </label>
                    </fieldset>
                    <fieldset>
                        <button type="submit" class="pure-button pure-button-primary">Create</button>
                    </fieldset>
                </form>
            </div>
    </div>
</body>
<#include "inc/foot.ftl">

</html>



