define(['jquery', 'caret'], function($) {

    var N_CELLS = 16, DELETE = 46, BS = 8, currentValue = "", input, cells = [], setReady;

    function setBoard(val) {
        var i, max = (val.length > N_CELLS) ? N_CELLS : val.length;
        for (i = 0; i < max; i++) {
            cells[i].val(val.charAt(i));
        }
        for (i = max; i < N_CELLS; i++) {
            cells[i].val("");
        }
    }

    function init() {
        currentValue = input.val();
        for (var i = 0; i < N_CELLS; i++) {
            cells.push($("#cell"+i));
        }
    }

    function isLetter(key) {
        var str = String.fromCharCode(key);
        return str.match(/[a-z]/i);
    }

    function insert(s, pos) {
        var value = currentValue.slice(0, pos) + s + currentValue.slice(pos);
        if (value.length > N_CELLS) {
            value = value.slice(0, N_CELLS);
        }
        currentValue = value;
    }

    // can't be botherered to sort out the jquery.caret plugin...
    function setCaretPosition(elemId, caretPos) {
        var elem = document.getElementById(elemId);

        if(elem != null) {
            if(elem.createTextRange) {
                var range = elem.createTextRange();
                range.move('character', caretPos);
                range.select();
            }
            else {
                if(elem.selectionStart) {
                    elem.focus();
                    elem.setSelectionRange(caretPos, caretPos);
                }
                else
                    elem.focus();
            }
        }
    }

    function setup(inputField, ready) {
        var key, caret, newCaret;

        input = $(inputField);
        init();

        input.keydown(function(e) {
            caret = input.caret();
            key = e.keyCode;
            if (isLetter(key) && caret.end == N_CELLS) {
                insert(String.fromCharCode(key).toLowerCase(), 0);
                input.val(currentValue);
                setBoard(currentValue);
                input.caret(1,1);
            } else if (isLetter(key) && caret.end >= 0) {
                insert(String.fromCharCode(key).toLowerCase(), caret.end);
                input.val(currentValue);
                setBoard(currentValue);
                newCaret = caret.end+1;
                setCaretPosition("in", newCaret);
            } else if (caret.end) {
                if (key == DELETE || key == BS) {
                    if (caret.start == caret.end && caret.end > 0) {
                        if (caret.end == currentValue.length) {
                            currentValue = currentValue.slice(0, caret.end-1);
                        } else {
                            currentValue = currentValue.slice(0, caret.end-1) + currentValue.slice(caret.end)
                        }
                        input.val(currentValue);
                        setBoard(currentValue);
                        input.caret(caret.end-1);
                    }
                }
            }
            setReady = ready;
            setReady();
            e.preventDefault();
        });

        $(".cell").keyup(function(e) {
            var val = "";
            for (var i = 0; i < N_CELLS; i++) {
                val += $("#cell"+i).val();
            }
            currentValue = val;
            input.val(currentValue);
            setReady();
        });
    }


    return {
        setup: setup,
        clear: function() {currentValue = ""; input.val(currentValue); input.focus();}
    };



});