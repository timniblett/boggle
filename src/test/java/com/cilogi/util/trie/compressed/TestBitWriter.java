// Copyright (c) 2013 Cilogi. All Rights Reserved.
//
// File:        TestBitWriter.java  (09/08/13)
// Author:      tim
//
// Copyright in the whole and every part of this source file belongs to
// Cilogi (the Author) and may not be used, sold, licenced, 
// transferred, copied or reproduced in whole or in part in 
// any manner or form or in or on any media to any person other than 
// in accordance with the terms of The Author's agreement
// or otherwise without the prior written consent of The Author.  All
// information contained in this source file is confidential information
// belonging to The Author and as such may not be disclosed other
// than in accordance with the terms of The Author's agreement, or
// otherwise, without the prior written consent of The Author.  As
// confidential information this source file must be kept fully and
// effectively secure at all times.
//


package com.cilogi.util.trie.compressed;

import org.junit.Before;
import org.junit.Test;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.junit.Assert.*;

public class TestBitWriter {
    static final Logger LOG = LoggerFactory.getLogger(TestBitWriter.class);


    public TestBitWriter() {
    }

    @Before
    public void setUp() {

    }

    @Test
    public void testEmpty() {
        BitWriter writer = new BitWriter();
        assertEquals("", writer.getData());
    }

    @Test
    public void testFirstBit() {
        BitWriter writer = new BitWriter();
        writer.write(0x1, 1);
        assertEquals("g", writer.getData());
    }

    @Test
    public void testLastBit() {
        BitWriter writer = new BitWriter();
        writer.write(0x20, 6);
        assertEquals("g", writer.getData());
    }

    @Test
    public void testSeveral() {
        BitWriter writer = new BitWriter();
        //writer.write(0, 1);
        writer.write(1, 1);
        writer.write(0, 1);
        writer.write(1, 1);
        writer.write(0, 1);
        assertEquals("o", writer.getData());
    }

    @Test
    public void testHighBit() {
        BitWriter writer = new BitWriter();
        int signOnly = 1 << 31;
        LOG.debug("as num " + signOnly + "as bits: " + Integer.toBinaryString(signOnly));
        writer.write(signOnly, 32);
        String s = writer.getData();
        LOG.debug("s = " + s);
    }

}