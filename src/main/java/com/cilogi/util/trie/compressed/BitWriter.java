// Copyright (c) 2013 Cilogi. All Rights Reserved.
//
// File:        BitWriter.java  (09/08/13)
// Author:      tim
//
// Copyright in the whole and every part of this source file belongs to
// Cilogi (the Author) and may not be used, sold, licenced, 
// transferred, copied or reproduced in whole or in part in 
// any manner or form or in or on any media to any person other than 
// in accordance with the terms of The Author's agreement
// or otherwise without the prior written consent of The Author.  All
// information contained in this source file is confidential information
// belonging to The Author and as such may not be disclosed other
// than in accordance with the terms of The Author's agreement, or
// otherwise, without the prior written consent of The Author.  As
// confidential information this source file must be kept fully and
// effectively secure at all times.
//


package com.cilogi.util.trie.compressed;

import com.google.common.collect.Maps;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.BitSet;
import java.util.Map;


public class BitWriter {
    static final Logger LOG = LoggerFactory.getLogger(BitWriter.class);

    private static final int CHAR_BITS = 6;
    private static final String BASE64 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_";

    private static final Map<Character, Integer> BASE64_CACHE = Maps.newHashMap();
    static {
        for (int i = 0; i < BASE64.length(); i++) {
            BASE64_CACHE.put(BASE64.charAt(i), i);
        }
    }

    private final BitSet bits;
    private int currentIndex;


    static int ORD(char c) {
        return BASE64_CACHE.get(c);
    }

    public BitWriter() {
        this.bits = new BitSet();
        currentIndex = 0;
    }

    /**
     * Write a number of bits (no more than 32)
     * @param data  The data, which is an int with bits set
     * @param numBits  The number of bits, starting from the least significant, so that
     *                 is numBits is 6, then we'll copy the least significant bits, starting
     *                 from the most significant.
     */
    public synchronized void write(int data, int numBits) {
        for (int i = numBits-1; i >= 0; i--) {
            int val = data & (1<<i);
            if (val != 0) {
                bits.set(currentIndex);
            }
            currentIndex++;
        }
    }

    /**
     * Get the set as Hex. If the number of bits written doesn't fit exactly
     * into the 6-bit Base64 digits then
     * @return A string of Hex characters representing the set.
     */
    public synchronized String getData() {
        StringBuilder s  = new StringBuilder();
        for (int index = 0; index < currentIndex; index += CHAR_BITS) {
            int val = 0;
            for (int i = index; i < index + CHAR_BITS; i++) {
                val <<= 1;
                if (bits.get(i)) {
                    val |= 0x1;
                }
            }
            s.append(BASE64.charAt(val));
        }
        return s.toString();
    }

    @Override
    public synchronized String toString() {
        return "bitset(" + bits + ")";
    }
}
