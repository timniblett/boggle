define(["boggle/state", "boggle/board"], function(State, Board) {

    function BoggleSolver(trie, values) {
        this.trie = trie;
        this.values = values;

        this.state = new State(trie.getRoot(), values);
        this.words = [];
    }


    BoggleSolver.prototype.solve = function() {
        var location, state;
        State.count = 0;
        for (location = 0; location < Board.size; location++) {
            state = new State(this.trie.getRoot(), this.values).newState(location);
            this.solveState(state);
        }
        this.wordsByLength = sortWords(this.words);
        this.count = State.count;
    }

    BoggleSolver.prototype.getWords = function() {
        return this.words;
    }

    BoggleSolver.prototype.solveState = function(state) {
        var i, next, newState, prefix, neighbours = Board.adjacent(state.location);
        for (i = 0; i < neighbours.length; i++) {
            next = neighbours[i];
            if (!state.hasVisited(next)) {
                newState = state.newState(next);
                if (newState.node && newState.isTerminal() && newState.prefix.length > 2) {
                    this.words.push(newState.prefix);
                }
                if (newState.node) {
                    this.solveState(newState);
                }
            }
        }
    }

    function sortWords(words) {
        var i, len, tmp = [];

        words.sort(function(a, b) {
            return b.length - a.length;
        });
        return split(words);
    }

    function split(orgWords) {
        var out = [], words = orgWords.slice(0);
        while (words.length > 0) {
            out.push(splitFirst(words));
        }
        return out;
    }

    function splitFirst(words) {
        var len = words[0].length, out = [];
        while (words.length && words[0].length === len) {
            out.push(words.shift());
        }
        return makeUnique(out.sort());
    }

    function makeUnique(sortedWords) {
        var i, out = [];
        for (i = 0; i < sortedWords.length; i++) {
            if (i == 0 || (i > 0 && sortedWords[i] != sortedWords[i-1])) {
                out.push(sortedWords[i]);
            }
        }
        return out;
    }

    return BoggleSolver;
});